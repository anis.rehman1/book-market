import 'package:book_selling_app/features/other/presentation/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'config/routes/app_routes.dart';
import 'config/util/AllBindings.dart';

class BookApp extends StatelessWidget {
  const BookApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Poppins'),
      getPages: AppRoutes.appRoutes,
      initialRoute: "splash",
      initialBinding: AllBindings(),
      unknownRoute: GetPage(
          name: "/splash",
          page: () =>  const SplashScreen()
      ),
    );
  }

}


