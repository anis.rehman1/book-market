import 'dart:convert';
import 'dart:isolate';
import 'package:book_selling_app/network/dio_service.dart';
import 'package:http/http.dart' as http;


class IsolateService{

  static void entryPoint(SendPort sendPort) async {

    var response  = await DioService().post("https://jsonplaceholder.typicode.com/posts");

    if (response.statusCode == 201) {
      sendPort.send(response.data); // Send data back to main isolate
    } else {
      sendPort.send(null); // Send null on error
    }
  }
}