import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DioService {
  late Dio _dio;
  static final DioService _singleton = DioService._internal();

  factory DioService() {
    return _singleton;
  }

  DioService._internal() {
    initializeDio();
  }

  Future<String?> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  Future<void> initializeDio() async {
    _dio = Dio(
      BaseOptions(
      ),
    );

    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) {
          // Do something before request is sent
          return handler.next(options); // continue
        },

        onError: (DioError e, handler) {
          // Do something on error
          return handler.next(e); // continue
        },
        // ...
      ),
    );
  }

  Future<Response> get(String url, {Map<String, dynamic>? params}) async {
    return _dio.get(url, queryParameters: params);
  }

  Future<Response> post(String url, {Map<String, dynamic>? data}) async {
    Map<String, dynamic> data = {
      "title": "foo",
      "body": "bar",
      "userId": 1,
    };
    return _dio.post(url, data: data);
  }

// Add other methods like put, delete, etc. as needed
}