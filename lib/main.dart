import 'package:flutter/material.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'app.dart';
import 'config/constants.dart';
import 'config/util/SharedPreferenceUtil.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // init Supabase
  await Supabase.initialize(
    url: supabaseUrl,
    anonKey: supabaseKey,
  );
  Preference.load();
  runApp(
      const BookApp(),
  );
}
