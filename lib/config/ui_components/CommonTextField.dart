import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'decoration.dart';
import 'input_decoration.dart';

class CommonTextField extends StatelessWidget {
  final String hintLabel;
  final TextEditingController controller;
  final Function(String?) onTextChanges;
  final Function()? onEditComplete;
  final Function(String?) onValidate;
  final String errorText;
  final TextInputType textInputType;
  final FocusNode? node;
  final Color borderColor;
  final Color fillColor;
  final bool filled;
  final bool enabled;
  final String labelText;
  final Color enableBorderColor;
  final Color focusBorderColor;
  final EdgeInsetsGeometry contentPadding;
  final double borderRadius;
  final TextStyle? labelTextStyleForTextField;
  final TextStyle? hintTextStyleForTextField;
  final Widget? suffixIcon;
  int? maxLines;
  int? minLines;
  int? maxLength;
  BorderSide? borderSide;
  InputBorder? border;
  InputBorder? enabledBorder;
  InputBorder? focusedBorder;

  CommonTextField({
    super.key,
    required this.hintLabel,
    required this.controller,
    required this.onValidate,
    required this.onTextChanges,
    this.node,
    this.errorText = '',
    this.textInputType = TextInputType.text,
    this.onEditComplete,
    this.filled = false,
    this.fillColor = Colors.transparent,
    this.enabled = true,
    this.borderColor = dividerColor,
    this.labelText = "",
    this.enableBorderColor = dividerColor,
    this.focusBorderColor = secondary,
    this.contentPadding = const EdgeInsets.fromLTRB(30, 0, 0, 0),
    this.borderRadius = 8,
    this.hintTextStyleForTextField,
    this.labelTextStyleForTextField,
    this.suffixIcon,
    this.minLines,
    this.maxLines,
    this.borderSide,
    this.border,
    this.focusedBorder,
    this.enabledBorder,
    this.maxLength,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: maxLines,
      minLines: minLines,
      focusNode: node,
      controller: controller,
      // onTapOutside: (_) {
      //   FocusManager.instance.primaryFocus?.unfocus();
      // },
      maxLength: maxLength,
      cursorColor: Colors.black,
      enabled: enabled,
      onFieldSubmitted: (value) {},
      onEditingComplete: () {
        if (onEditComplete != null) {
          onEditComplete!();
        } else {
          node?.unfocus();
        }
      },
      onChanged: onTextChanges,
      decoration: inputDecoration(
        suffixIcon: suffixIcon,
        borderRadius: borderRadius,
        prefixText: hintLabel,
        borderColor: borderColor,
        enabledBorderColor: enableBorderColor,
        focusedBorderColor: focusBorderColor,
        filled: filled,
        fillColor: fillColor,
        labelText: labelText,
        contentPadding: contentPadding,
        hintTextStyle: hintTextStyleForTextField ?? hintTextStyle,
        labelTextStyle: labelTextStyleForTextField ?? labelTextStyle,
        borderSide: borderSide,
        border: border,
        focusedBorder: focusedBorder,
        enabledBorder: enabledBorder,
      ),
      textInputAction: TextInputAction.done,
      keyboardType: textInputType,
      autofillHints: [hintLabel],
      validator: (String? value) {
        onValidate(value);
        return null;
      },
    );
  }
}
