import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import 'decoration.dart';

class CommonOTPField extends StatelessWidget {
  final Function(String) onChange;
  final Function(String) onComplete;
  final bool isError;

  const CommonOTPField({super.key, required this.onChange, required this.onComplete, required this.isError});

  @override
  Widget build(BuildContext context) {
    return Form(
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 0.0, horizontal: 0),
            child: PinCodeTextField(
              keyboardType: TextInputType.number,
              length: 6,
              onChanged: (String value) {
                onChange(value);
              },
              onCompleted: (value) {
                onComplete(value);
              },
              appContext: context,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              cursorColor: black,
              pinTheme: PinTheme(
                  shape: PinCodeFieldShape.box,
                  borderRadius: BorderRadius.circular(5),
                  fieldHeight: 50,
                  fieldWidth: MediaQuery.of(context).size.width / 9,
                  activeFillColor: primary,
                  activeColor: isError ? buttonErrorBorderColor : secondary,
                  inactiveColor: greyBgColor,
                  selectedColor: secondary,
                  selectedFillColor: secondary),
            )));
  }
}
