import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'CommonText.dart';
import 'decoration.dart';


class CommonCheckbox extends StatelessWidget {
  final Function() onChange;
  final Function(bool?)? onValueChanged;
  final String? text;
  final bool isChecked;
  final bool? isTextGreaterThenOneLine;

  const CommonCheckbox(
      {super.key, required this.onChange,
      this.text,
      this.isChecked = false,
        this.isTextGreaterThenOneLine = false,
      this.onValueChanged});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: isTextGreaterThenOneLine == true ? CrossAxisAlignment.start : CrossAxisAlignment.center,
      children: [
        SizedBox(
            height: 24.0,
            width: 24.0,
            child: Checkbox(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    side: const BorderSide(
                      color: greyBgColor,
                      width: 0.1,
                    )),
                value: isChecked,
                checkColor: primary,
                activeColor: secondary,
                onChanged: (value) {
                  onChange();
                  if (onValueChanged != null) {
                    onValueChanged!(value);
                  }
                })),
        sizeWidthBox10,
        text != null
            ? Expanded(
                child: CommonText(
                value: text!,
                fontSize: 14,
                textColor: disableGrayColor,
                  textAlign: TextAlign.left,
              ))
            : Container(),
      ],
    );
  }
}
