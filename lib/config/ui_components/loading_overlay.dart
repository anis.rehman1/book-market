
import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'CommonProgressBar.dart';

class LoadingOverlay extends StatelessWidget {
  const LoadingOverlay({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ModalBarrier(
          dismissible: false,
          color: primary.withOpacity(0.1),
        ),
        Center(
          child: CommonProgressBar(
            colorAndroid: secondary,
            colorIos: secondary,
          ),
        )
      ],
    );
  }
}

