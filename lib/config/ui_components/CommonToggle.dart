import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'CommonText.dart';
import 'FontsConstant.dart';
import 'decoration.dart';

class CommonToggle<T> extends StatefulWidget {
  final List<T> values;
  final List<T>? valuesSubheading;
  final ValueChanged onToggleCallback;
  final Color backgroundColor;
  final Color buttonColor;
  final Color textColor;
  final Color unselectedTabTextColor;
  final String? fontFamily;
  final double? fontSize;
  final FontWeight fontWeight;
  final double? viewWidth;
  final double? whiteViewWidth;
  final double? borderRadius;
  bool? initialPosition;
  final double? height;
 final EdgeInsetsGeometry? margin;

  CommonToggle({super.key, 
    required this.values,
    this.valuesSubheading,
    required this.onToggleCallback,
    this.backgroundColor = greyBgColor,
    this.buttonColor = primary,
    this.textColor = secondary,
    this.unselectedTabTextColor = subTitleColor,
    this.fontWeight = FontWeight.w500,
    this.fontSize = 14,
    this.fontFamily = Medium,
    this.viewWidth,
    this.whiteViewWidth,
    this.borderRadius,
    this.initialPosition = false,
    this.height = 40,
    this.margin = const EdgeInsets.all(30),
  });

  @override
  _CommonToggleState createState() => _CommonToggleState();
}

class _CommonToggleState extends State<CommonToggle> {
  bool initialPosition = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialPosition = widget.initialPosition!;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.viewWidth ?? MediaQuery.of(context).size.width / 2,
      height: widget.height,
      margin: widget.margin,
      child: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              // print("before = ${widget.initialPosition}");
              // widget.initialPosition = null;
              initialPosition = !initialPosition;
              var index = 0;
              if (!initialPosition) {
                index = 1;
              }
              widget.onToggleCallback(widget.values[index]);
              setState(() {

                // if(widget.initialPosition != null){
                //   initialPosition = widget.initialPosition!;
                //   widget.initialPosition = null;
                // }

              });
             // print("after = ${widget.initialPosition}");

            },
            child: Container(
              width: widget.viewWidth ?? MediaQuery.of(context).size.width / 2,
              height: widget.height,
              decoration: ShapeDecoration(
                color: widget.backgroundColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(widget.borderRadius ?? 8),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: List.generate(
                  widget.values.length,
                  (index) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 25),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CommonText(
                          textColor: widget.unselectedTabTextColor,
                          fontWeight: widget.fontWeight,
                          fontSize: widget.fontSize,
                          value: widget.values[index].toString().split('.').last,
                        ),
                        widget.valuesSubheading != null ?
                        Padding(
                          padding: const EdgeInsets.only(top: 2.0),
                          child: CommonText(
                            value:  initialPosition
                                ? widget.valuesSubheading == null ? "" : widget.valuesSubheading![1]
                                : widget.valuesSubheading == null ? "" : widget.valuesSubheading![0],
                            textColor: widget.unselectedTabTextColor,
                            fontWeight: FontWeight.w400,
                            fontSize: 7,
                            fontFamilyName: widget.fontFamily,
                          ),
                        ) : const SizedBox()
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          AnimatedAlign(
            duration: const Duration(milliseconds: 250),
            curve: Curves.decelerate,
            alignment:
            (initialPosition) ? Alignment.centerLeft : Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Container(
                width: widget.whiteViewWidth ?? MediaQuery.of(context).size.width / 3.5,
                height: widget.height! - 5,
                decoration: ShapeDecoration(
                  color: widget.buttonColor,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(widget.borderRadius ?? 8),
                  ),
                ),
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CommonText(
                      value:  initialPosition
                          ? widget.values[0].toString().split('.').last
                          : widget.values[1].toString().split('.').last,
                      textColor: widget.textColor,
                      fontWeight: widget.fontWeight,
                      fontSize: widget.fontSize,
                      fontFamilyName: widget.fontFamily,
                    ),
                    widget.valuesSubheading != null ?
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0),
                      child: CommonText(
                        value:  initialPosition
                            ? widget.valuesSubheading == null ? "" : widget.valuesSubheading![0]
                            : widget.valuesSubheading == null ? "" : widget.valuesSubheading![1],
                        textColor: widget.textColor,
                        fontWeight: FontWeight.w400,
                        fontSize: 8,
                        fontFamilyName: widget.fontFamily,
                      ),
                    ) : const SizedBox()
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
