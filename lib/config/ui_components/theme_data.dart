import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';

import 'FontsConstant.dart';
import 'decoration.dart';


const primary = Color(0xffffffff);
const secondary = Color(0xFF4E75FF);
//const secondary = Color(0xff45b97c);//#45B97C

class ThemeManager {
  static const String PREF_THEME_MODE = "pref_theme_mode";
  static ThemeMode? _preferredThemeMode;

  ThemeMode get currentTheme {
    if (_preferredThemeMode != null) {
      return _preferredThemeMode!;
    } else {
      return _isDarkMode ? ThemeMode.dark : ThemeMode.light;
    }
  }

  /// This method is to toggle from the current theme,
  /// right now it toggles between [ThemeMode.light] and
  /// [ThemeMode.dark]
  void toggleTheme() {
    _preferredThemeMode = currentTheme.reverse();
    // this will rebind the view with new theme
    WidgetsBinding.instance.performReassemble();
  }

  bool get _isDarkMode {
    var brightness = SchedulerBinding.instance.window.platformBrightness;
    bool isDarkMode = brightness == Brightness.dark;
    return isDarkMode;
  }

  static ThemeData get lightTheme {
    return ThemeData(
        useMaterial3: false,
        primaryColor: primary,
        colorScheme: const ColorScheme.light(primary: primary, secondary: secondary),
        textTheme: const TextTheme(
          bodyLarge: titleTextStyle,
          bodyMedium: TextStyle(color: black),
          bodySmall: TextStyle(color: black),
        ),
        primaryTextTheme: const TextTheme(
          bodyLarge: titleTextStyle,
          bodyMedium: TextStyle(color: black),
          bodySmall: TextStyle(color: black),
        ),
        scaffoldBackgroundColor: primary,
        fontFamily: FontFamily,
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: WidgetStateProperty.all(secondary),
            textStyle: WidgetStateProperty.all(const TextStyle(color: black)),
            shape: WidgetStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
            ),
          ),
        ),
        buttonTheme: ButtonThemeData(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          buttonColor: secondary,
        ));
  }

  static ThemeData get darkTheme {
    return ThemeData(
      useMaterial3: false,
      primaryColor: secondary,
      colorScheme: const ColorScheme.dark(primary: secondary, secondary: primary),
      textSelectionTheme: const TextSelectionThemeData(cursorColor: secondary),
      textTheme: const TextTheme(
          bodyLarge: titleTextStyle,
          bodyMedium: TextStyle(color: Colors.white),
          bodySmall: TextStyle(color: Colors.white),
          titleSmall: TextStyle(color: Colors.white),
          titleMedium: TextStyle(color: Colors.white),
          titleLarge: TextStyle(color: Colors.white)),
      scaffoldBackgroundColor: primary,
      fontFamily: 'Gotham',
      appBarTheme: const AppBarTheme(
          backgroundColor: secondary,
          actionsIconTheme: IconThemeData(color: secondary),
          elevation: 0.0,
          systemOverlayStyle: SystemUiOverlayStyle.light),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: WidgetStateProperty.all(Colors.white),
          shape: WidgetStateProperty.all(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
          ),
        ),
      ),
      buttonTheme: ButtonThemeData(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
        buttonColor: secondary,
      ),
    );
  }
}

extension ReverseThemeMode on ThemeMode {
  ThemeMode reverse() {
    var themeMode = ThemeMode.system;
    switch (this) {
      case ThemeMode.light:
        themeMode = ThemeMode.dark;
        break;
      case ThemeMode.dark:
        themeMode = ThemeMode.light;
        break;
      case ThemeMode.system:
        themeMode = ThemeMode.light;
        break;
    }
    return themeMode;
  }
}

extension ThemeModeValue on int {
  ThemeMode? toThemeModeValue() {
    ThemeMode? themeMode;
    switch (this) {
      case 0:
        themeMode = ThemeMode.light;
        break;
      case 1:
        themeMode = ThemeMode.dark;
        break;
    }
    return themeMode;
  }
}

extension ThemeModeIntValue on ThemeMode {
  int toIntValue() {
    switch (this) {
      case ThemeMode.light:
        return 0;
      case ThemeMode.dark:
        return 1;
      case ThemeMode.system:
        return -1;
    }
  }
}
