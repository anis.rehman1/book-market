import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'CommonText.dart';
import 'FontsConstant.dart';
import 'decoration.dart';

class CommonButtonWithBorder extends StatelessWidget {
  final Function() onClick;
  final String text;
  final bool isEnabled;
  final Widget? rightIcon;
  final Color? buttonColor;

  const CommonButtonWithBorder({
    super.key,
    required this.onClick,
    this.isEnabled = true,
    required this.text,
    this.rightIcon,
    this.buttonColor,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: isEnabled
          ? () {
              onClick();
            }
          : null,
      style: ButtonStyle(
        backgroundColor: WidgetStateProperty.all(Colors.white),
        maximumSize: WidgetStateProperty.all(
          const Size.fromHeight(48),
        ),
        minimumSize: WidgetStateProperty.all(
          const Size.fromHeight(48),
        ),
        shape: WidgetStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40.0),
            side: BorderSide(color: buttonColor ?? buttonBgColor),
          ),
        ),
      ),
      child: Padding(
        padding: maxPaddingAll,
        child: CommonText(
          value: text,
          textColor: secondary,
          fontWeight: FontWeight.w500,
          fontSize: 14,
          fontFamilyName: Medium,
        ),
      ),
    );
  }
}
