import 'package:flutter/material.dart';

import 'FontsConstant.dart';
import 'decoration.dart';

class CommonText extends StatelessWidget {
  final String value;
  final double? fontSize;
  final Color? textColor;
  final FontWeight fontWeight;
  final TextAlign? textAlign;
  final String? fontFamilyName;
  final int? maxLine;
  final TextDecoration? textDecoration;
  final TextOverflow? textOverFlow;

  const CommonText({
    super.key,
    required this.value,
    this.fontSize = fontSize14,
    this.textColor = subTitleColor,
    this.fontWeight = FontWeight.w400,
    this.textAlign,
    this.fontFamilyName = FontFamily,
    this.maxLine,
    this.textDecoration,
    this.textOverFlow,
  });

  @override
  Widget build(BuildContext context) {
    return Text(value,
        softWrap: true,
        maxLines: maxLine,
        textAlign: textAlign,
        style: subtitleTextStyle.copyWith(
          decoration: textDecoration,
          fontSize: fontSize,
          color: textColor,
          fontWeight: fontWeight,
          fontFamily: fontFamilyName,
          overflow: textOverFlow,
        ));
  }
}
