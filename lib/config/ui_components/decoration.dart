import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'FontsConstant.dart';

const Color primaryColor = Color(0xFF4E75FF);
const Color secondry = Color(0xFFC63454);
const Color accentColor = Color(0xFFF7D27F);
const Color textColor = Color(0xFF333333);
// Add more custom colors as needed
// all the colors used used in app are placed here.
const white = Color(0xffF5F8FD);
const titleColor = Color(0xff000000);
const buttonBgColor = secondary;
const black = Color(0xff000000);
const grayTextColorLight = Color(0xffC6C6C6);
const buttonErrorBorderColor = Color(0xffd14951);
const passwordEyeColor = Color(0xff7f7f7f);
const subTitleColor = Color(0xff666666);
const buttonDisableColor = Color(0xff999999);
const buttonDisableBgColor = Color(0xffe6e6e6);
const greyBgColor = Color(0xffe6e6e6);
const lightGrayBackground = Color(0xFFF8F8F8);
const successColor = Color(0xff2f8f61);
const sideNavigationBarGradientStartColor = Color(0x6C000000);
const sideNavigationBarGradientEndColor = Color(0xff000000);
const dividerColor = Color(0xffBABABA);
const textFieldBgColor = Color(0xffE9E9EA);
const textColorBlack = Color(0xff112F41);
const disableGrayColor = Color(0xff586D7A);
const infoBgColor = Color(0xffE7EAEC);
const appBarGradiantColor1 = Color(0xff004144);
const appBarGradiantColorSplash = Color(0xff002f36);
const appBarGradiantColors2 = Color(0xff012d2f);
const appBarGradiantColor2 = Color(0xff183547);
const errorColor = Color(0xffC54F56);
const errorBgColor = Color(0x0fc54f56);
const dropDownGreyColor = Color(0xffE9E9EA);
const dropDownCloseColor = Color(0xff70828D);
const dropDownArrowColor = Color(0xff70828D);
const stepperColor = Color(0xff45b97c); //45B97C
const dropDownBgColor = Color(0xffFCFCFC);
const compareTableColor = Color(0xffECF8F2);
const vipColor = Color(0xffE99343);
const vipFillColor = Color(0x0fe99343);
const indicatorColor = Color(0xffB5E3CB);
const discountBoxColor = Color(0xff9179CD);
const saveBgBoxColor = Color(0x0f9179cd);
const carDisableBgColor = Color(0xffE9E9EA);
const swapCarDetailHeaderBgColor = Color(0x14e9e9ea);
const weekendDateColor = Color(0xff32A669);
const splashGradiantColor = Color(0xff112F41);

const continueWithColor = Color(0xff586D7A);
const down_arrow_color = Color(0xff99BDBE);
const scaffold_text_color = Color(0xffDAF1E5);
const carListLabelColor = Color(0xffA2DCBE);
const lightSuccessBackground = Color(0xffECF8F2);
const carDisableTextColor = Color(0xff888888);
const offerBgColor = Color(0xff9179CD);
const dividerColorForPriceBreakDown = Color(0xff294454);
const priceBreakDownBg = Color(0xff415967);
const addExtrasScaffoldTextColor = Color(0xffB8C1C6);
const searchFieldBorder = Color(0xffD9D9D9);
const rangeDisableColor = Color(0xffA0ACB3);
const successScreenFillColor = Color(0x14e9e9ea);
const successScreenBorderColor = Color(0x29ffffff);
const weekendItemBgColor = Color(0x00f5f5f5);
const selectedWeekendItemColor = Color(0xffF4FBF7);
const perDayTextColor = Color(0xff379463);
const homeSecondaryColor = Color(0xff07333A);
// all the paddings are kept here.
const minPaddingAll = EdgeInsets.all(5);
const avgPaddingAll = EdgeInsets.all(10);
const maxPaddingAll = EdgeInsets.all(12);
const ultraMaxPaddingAll = EdgeInsets.all(20);
const minPaddingHorizontal = EdgeInsets.symmetric(horizontal: 5);
const avgPaddingHorizontal = EdgeInsets.symmetric(horizontal: 10);
const maxPaddingHorizontal = EdgeInsets.symmetric(horizontal: 15);
const ultraMaxPaddingHorizontal = EdgeInsets.symmetric(horizontal: 20);
const minPaddingVertical = EdgeInsets.symmetric(vertical: 5);
const avgPaddingVertical = EdgeInsets.symmetric(vertical: 10);
const maxPaddingVertical = EdgeInsets.symmetric(vertical: 15);
const ultraMaxPaddingVertical = EdgeInsets.symmetric(vertical: 20);

// fontSize
const fontSize12 = 12.0;
const fontSize14 = 14.0;
const fontSize16 = 16.0;
const fontSize20 = 20.0;
const fontSize28 = 28.0;

// all sizedboxes for showing blank spaces between widgets.
const sizeBox150 = SizedBox(
  height: 150,
);
const sizeBox100 = SizedBox(
  height: 100,
);
const sizeBox70 = SizedBox(
  height: 70,
);
const sizeBox60 = SizedBox(
  height: 60,
);
const sizeBox50 = SizedBox(
  height: 50,
);
const sizeBox40 = SizedBox(
  height: 40,
);
const sizeBox45 = SizedBox(
  height: 45,
);
const sizeBox35 = SizedBox(
  height: 35,
);
const sizeBox30 = SizedBox(
  height: 30,
);
const sizeBox25 = SizedBox(
  height: 25,
);
const sizeBox24 = SizedBox(
  height: 24,
);
// const sizeBox26 = SizedBox(
//   height: 34.iY,
// );
const sizeBox20 = SizedBox(
  height: 20,
);
const sizeBox12 = SizedBox(
  height: 12,
);
const sizeBox15 = SizedBox(
  height: 15,
);
const sizeBox10 = SizedBox(
  height: 10,
);
const sizeBox8 = SizedBox(
  height: 8,
);
const sizeBox5 = SizedBox(
  height: 5,
);
const sizeBox2 = SizedBox(
  height: 2,
);

// width size box
const sizeWidthBox2 = SizedBox(
  width: 2,
);
const sizeWidthBox5 = SizedBox(
  width: 5,
);
const sizeWidthBox8 = SizedBox(
  width: 8,
);
const sizeWidthBox10 = SizedBox(
  width: 10,
);
const sizeWidthBox15 = SizedBox(
  width: 15,
);
const sizeWidthBox20 = SizedBox(
  width: 20,
);
// styles for texts
const h1texts = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w700,
);
const h2texts = TextStyle(
  fontSize: 15,
  fontWeight: FontWeight.w600,
);
const h3texts = TextStyle(
  fontSize: 12,
  fontWeight: FontWeight.w500,
);
const h4texts = TextStyle(
  fontSize: 10,
  fontWeight: FontWeight.w500,
);

const disableButtonTextStyle = TextStyle(
  fontSize: fontSize16,
  fontWeight: FontWeight.normal,
  color: buttonDisableColor,
);

const titleTextStyle = TextStyle(
  fontSize: fontSize28,
  fontWeight: FontWeight.bold,
  color: titleColor,
  fontFamily: FontFamily,
);

const subtitleTextStyle = TextStyle(
  fontSize: fontSize14,
  fontWeight: FontWeight.normal,
  color: subTitleColor,
  fontFamily: FontFamily,
);

const labelTextStyle = TextStyle(
  color: disableGrayColor,
  fontWeight: FontWeight.w500,
  fontSize: 14,
  fontFamily: FontFamily,
);

const hintTextStyle = TextStyle(
  color: textColorBlack,
  fontWeight: FontWeight.w400,
  fontSize: 10,
  fontFamily: FontFamily,
);
