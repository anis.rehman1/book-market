import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'decoration.dart';

InputDecoration inputDecoration({
  String prefixText = "",
  Color enabledBorderColor = secondary,
  Color focusedBorderColor = secondary,
  Color borderColor = secondary,
  Color fillColor = Colors.transparent,
  TextStyle hintTextStyle = hintTextStyle,
  bool filled = false,
  TextStyle labelTextStyle = labelTextStyle,
  EdgeInsetsGeometry contentPadding = const EdgeInsets.fromLTRB(30, 0, 0, 0),
  double borderRadius = 12,
  String labelText = "",
  Widget? suffixIcon,
  BorderSide? borderSide,
  InputBorder? border,
  InputBorder? enabledBorder,
  InputBorder? focusedBorder,
}) {
  return InputDecoration(
    alignLabelWithHint: true,
    hintStyle: hintTextStyle,
    hintText: prefixText,
    labelText: labelText,
    contentPadding: contentPadding,
    fillColor: fillColor,
    filled: filled,
    suffixIcon: suffixIcon,
    labelStyle: labelTextStyle,
    focusedBorder: focusedBorder ??
        OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius),
          borderSide: BorderSide(
            color: focusedBorderColor,
          ),
        ),
    enabledBorder: enabledBorder ??
        OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius),
          borderSide: BorderSide(
            color: enabledBorderColor.withOpacity(0.2),
          ),
        ),
    border: border ??
        OutlineInputBorder(
          borderRadius: BorderRadius.circular(borderRadius),
          borderSide: borderSide ??
              BorderSide(
                color: borderColor,
              ),
        ),
  );
}

BoxDecoration boxDecoration(
    {String prefixText = "",
    Color borderColor = secondary,
    double? borderRadius}) {
  return BoxDecoration(
      border: Border.all(
        color: borderColor,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(borderRadius ?? 50),
      ),
      shape: BoxShape.rectangle);
}

BoxDecoration boxDecorationWithFillColor(
    {String prefixText = "",
    Color borderColor = secondary,
    Color bgColor = compareTableColor,
    double? borderRadius}) {
  return BoxDecoration(
      border: Border.all(
        color: borderColor,
      ),
      color: bgColor,
      borderRadius: BorderRadius.all(
        Radius.circular(borderRadius ?? 50),
      ),
      shape: BoxShape.rectangle);
}
