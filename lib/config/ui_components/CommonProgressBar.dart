import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommonProgressBar extends StatelessWidget {
  final Color? colorIos;
  final Color? colorAndroid;

  const CommonProgressBar._({this.colorIos, this.colorAndroid});

  factory CommonProgressBar({colorIos, colorAndroid}) {
    return CommonProgressBar._(
      colorIos: colorIos,
      colorAndroid: colorAndroid,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: (Platform.isIOS)
          ? Transform.scale(
              scale: 1.4,
              child: ColorFiltered(
                colorFilter: ColorFilter.mode(
                  colorIos ?? Colors.white,
                  BlendMode.srcATop,
                ),
                child: const CupertinoActivityIndicator(),
              ),
            )
          : CircularProgressIndicator(
              color: colorAndroid ?? Colors.blue,
            ),
    );
  }
}
