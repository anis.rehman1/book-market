// import 'package:flutter/material.dart';
// import 'dart:math' as math;
//
// class DashedCircle extends CustomPainter {
//   final Color color;
//   final double strokeWidth;
//   final double gap;
//
//   DashedCircle({
//     this.color = Colors.black,
//     this.strokeWidth = 1.0,
//     this.gap = 5.0,
//   });
//
//   @override
//   void paint(Canvas canvas, Size size) {
//     final paint = Paint()
//       ..color = color
//       ..strokeWidth = strokeWidth
//       ..style = PaintingStyle.stroke;
//
//     final path = Path();
//     path.addCircle(Offset(size.width / 2, size.height / 2), size.width / 2);
//
//     final pathLength = path.length;
//     double start = 0.0;
//     for (int i = 0; i < pathLength; i += strokeWidth + gap) {
//       final end = math.min(start + strokeWidth, pathLength);
//       canvas.drawPath(path.subpath(start, end), paint);
//       start = end + gap;
//     }
//   }
//
//   @override
//   bool shouldRepaint(CustomPainter oldDelegate) => true;
// }