import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:flutter/material.dart';

import 'CommonText.dart';
import 'FontsConstant.dart';
import 'decoration.dart';

class CommonButton extends StatelessWidget {
  final Function() onClick;
  final String text;
  final bool isEnabled;
  final Widget? rightIcon;
  final Color? buttonColor;

  const CommonButton({
    super.key,
    required this.onClick,
    this.isEnabled = true,
    required this.text,
    this.rightIcon,
    this.buttonColor,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: isEnabled
          ? () {
              onClick();
            }
          : null,
      style: isEnabled
          ? ButtonStyle(
              backgroundColor:
                  WidgetStateProperty.all(buttonColor ?? buttonBgColor),
              maximumSize: WidgetStateProperty.all(const Size.fromHeight(48)),
              minimumSize: WidgetStateProperty.all(const Size.fromHeight(48)),
              shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(color: buttonColor ?? buttonBgColor))))
          : ButtonStyle(
              backgroundColor: WidgetStateProperty.all(buttonDisableBgColor),
              maximumSize: WidgetStateProperty.all(const Size.fromHeight(48)),
              minimumSize: WidgetStateProperty.all(const Size.fromHeight(48)),
              shape: WidgetStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: const BorderSide(color: buttonDisableBgColor)))
              //  maximumSize: MaterialStateProperty.all(Size.fromHeight(78))
              ),
      child: Padding(
        padding: maxPaddingAll,
        child: CommonText(
          value: text,
          textColor: isEnabled ? primary : buttonDisableColor,
          fontWeight: FontWeight.w500,
          fontSize: 14,
          fontFamilyName: Medium,
        ),
      ),
    );
  }
}
