
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class GetImage{

  static svgFromAssets(String path, double width, double height){
    return SvgPicture.asset(path,
    width: width,
    height: height);
  }

  fromAssets(String path){
    return AssetImage("assets/$path");
  }

  static cachedImageFromNetwork(String path, double width, double height){
    return CachedNetworkImage(
      width: width,
      height: height,
      imageUrl: path,
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }

}