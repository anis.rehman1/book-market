
import 'package:flutter/cupertino.dart';
import 'package:geocoding/geocoding.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

const supabaseUrl = 'https://lygmlpqoxuonspxaeoio.supabase.co';
const supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Imx5Z21scHFveHVvbnNweGFlb2lvIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTMyOTczNjksImV4cCI6MjAyODg3MzM2OX0.z6_WYefMG5v5VOa5KZo9lGynwVim_l_jSPjGA6ZYFPg";
final supabase = Supabase.instance.client;

String? userName;

double? deviceWidth;
double? deviceHeight;
BuildContext? buildContext;

Placemark? userAddress;

double getDeviceWidth(){
  return deviceWidth ?? 0;
}

double getDeviceHeight(){
  return deviceHeight ?? 0;
}