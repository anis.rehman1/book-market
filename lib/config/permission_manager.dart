import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io' show Platform;

class PermissionManager {
  late final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

  Future<bool> requestPermission({
    bool camera = false,
    bool storage = false,
    bool location = false,
    Function? success,
    Function(Permission permission, PermissionStatus status)? fail,
  }) async {
    late List<Permission> requestList;

    if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      int sdkInt = androidDeviceInfo.version.sdkInt;
      requestList = [
        if (location) Permission.locationWhenInUse,
        if (location) Permission.location,
        if (storage && sdkInt < 33) Permission.storage,
        if (storage && sdkInt >= 33) Permission.photos,
        //if (storage && sdkInt >= 33) Permission.videos,
        if (camera) Permission.camera,
      ];
    } else if (Platform.isIOS) {
      requestList = [
        if (location) Permission.locationWhenInUse,
        if (location) Permission.location,
        if (storage) Permission.photos,
        if (camera) Permission.camera,
      ];
    }

    debugPrint("permission requested: ${requestList.join(",").toString()}");
    //FirebaseCrashlytics.instance.log("permission requested: ${requestList.join(",").toString()}");

    Map<Permission, PermissionStatus> statuses = await requestList.request();

    bool isPermissionAdded = true;

    for (var request in requestList) {
      debugPrint("$request ${statuses[request]}");
      if (statuses[request] == PermissionStatus.denied ||
          statuses[request] == PermissionStatus.permanentlyDenied) {
        //statuses[request] != PermissionStatus.limited
        isPermissionAdded = false;
        fail?.call(request, statuses[request]!);
        //FirebaseCrashlytics.instance.log("permission requested FAILED REQ: $request");
        //FirebaseCrashlytics.instance.log("permission requested FAILED STATUS: ${statuses[request]}");

        break;
      }
    }
    if (isPermissionAdded) success?.call();
    return isPermissionAdded;
  }

  isPermanentDenied({bool location = false}) async {
    if (location) {
      debugPrint(
          "Location is allowed: ${await Permission.location.isPermanentlyDenied} ");
      return await Permission.location.isPermanentlyDenied;
    } else {
      debugPrint("UNKNOWN permission request");
      return false;
    }
  }

  retryForPermission(Permission permission) async {
    bool canShowRational = await permission.shouldShowRequestRationale;
    debugPrint("Retry permission: $canShowRational");
  }

  Future<bool> isGpsEnb() async {
    return await Permission.locationWhenInUse.serviceStatus.isEnabled;
  }
}
