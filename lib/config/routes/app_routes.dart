import 'package:book_selling_app/features/authorization/presentation/pages/VerifyOTPScreen.dart';
import 'package:book_selling_app/features/authorization/presentation/pages/login_screen.dart';
import 'package:book_selling_app/features/books/data/repositories/BookDetailPage.dart';
import 'package:book_selling_app/features/books/presentation/pages/AllBooksScreen.dart';
import 'package:book_selling_app/features/books/presentation/providers/HomeController.dart';
import 'package:book_selling_app/features/location/presentation/screen/PickLocationScreen.dart';
import 'package:book_selling_app/features/other/presentation/pages/splash_screen.dart';
import 'package:get/get.dart';

import '../../features/authorization/data/repositories/UserRepositoryImp.dart';
import '../../features/authorization/domain/usecases/GetUserUseCase.dart';
import '../../features/authorization/domain/usecases/UpdateLatLon.dart';
import '../../features/authorization/domain/usecases/add_user_usecase.dart';
import '../../features/books/data/repositories/book_repository_imp.dart';
import '../../features/books/presentation/pages/HomeScreen.dart';
import '../../features/location/data/repository/PinCodeRepositoryImpl.dart';

class AppRoutes {
  static String get splash => '/splash';

  static String get login => '/login';

  static String get home => '/home';

  static String get bookDetails => '/bookDetails';

  static String get verifyOtp => '/verify_otp';

  static String get pickLocation => '/pickLocation';

  static String get allBooks => '/allBooks';

  static List<GetPage<dynamic>> appRoutes = [
    GetPage(name: splash, page: () => const SplashScreen()),
    GetPage(name: login, page: () => LoginScreen()),
    GetPage(name: verifyOtp, page: () => const VerifyOTPScreen()),
    GetPage(name: home, page: () => const HomeScreen(), binding: HomeControllerBinding()),
    GetPage(name: bookDetails, page: () => const BookDetailScreen()),
    GetPage(name: pickLocation, page: () => const PickLocationScreen()),
    GetPage(name: allBooks, page: () => const AllBooksScreen()),
  ];
}

class HomeControllerBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => HomeController(
        AddUser(UserRepositoryImp()),
        GetUser(UserRepositoryImp()),
        BookRepositoryImp(),
        LocalityRepositoryImpl(),
        UpdateLatLon(UserRepositoryImp()))); // Define your provider here
  }
}
