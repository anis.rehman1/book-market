
class Validator {
  //super(regexSource: r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
  // This function checks for the email regex and return the result as boolean value
  static bool isEmailValid({required String emailID}) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(emailID)) {
      return false;
    } else {
      return true;
    }
      return false;
  }

  // This function checks for the alphanumeric regex which includes only alphabets & numbers and return the result as boolean value
  static bool isAlphaNumeric({required String text}) {
    Pattern pattern = RegExp(r'^[a-zA-Z0-9]+$');
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(text)) {
      return false;
    } else {
      return true;
    }
  }

/*
This function checks for the alphanumeric with special character regex and return the result as boolean value.
we can also add more special characters as per need
*/
  static bool isAlphaNumericWithSpecialCharacters({required String text}) {
    Pattern pattern =
        RegExp(r'^[a-zA-Z0-9_\-=@,\.;]+$'); // alphanumeric and _-=@,.;
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(text)) {
      return false;
    } else {
      return true;
    }
  }

  static Future<bool?> isValidMobileNumber(
      {required String mobileNumber, required String isoCode}) async {
    bool? isValid = true;
    return isValid;
  }


  static Future<bool?> isValidInternationalMobileNumber(
      {required String mobileNumber, required String isoCode}) async {
    if (!mobileNumber.startsWith("0")) {
      bool? isValid = true;
      return isValid;
    }
    return false;
  }

  static bool isValidEmiratesID({required String emiratesID}) {
    final regex = RegExp('^[0-9]{3}-d?[0-9]{4}-d?[0-9]{7}-d?[0-9]{1}\$');
    if (!regex.hasMatch(emiratesID)) {
      return false;
    } else {
      return true;
    }
  }

  static bool isValidPassword({required String password}) {
    final validCharacters = RegExp(r'^[a-zA-Z0-9_\-=@,\.;]+$');
    bool isAtLeast8 = password.length >= 8;
    bool hasSpecial = validCharacters.hasMatch(password);
    bool hasUpperCase = password != password.toLowerCase();
    return isAtLeast8 && hasSpecial && hasUpperCase;
  }

  // This function checks for the numberic and return the result as boolean value
  static bool isNumeric({required String text}) {
    if (text == null) {
      return false;
    }
    return double.tryParse(text) != null;
  }

  static bool isEmpty({required String? string}) {
    return string == null || string.isEmpty;
  }

  static bool isEqual({required String param1, required String param2}) {
    return !(param1.isEmpty || param2.isEmpty) &&
        param1.toLowerCase().trim() == (param2.toLowerCase().trim());
  }

  // This function checks for the alphanumeric regex which includes only alphabets & numbers and return the result as boolean value
  static bool isAlphabetOnly({required String text}) {
    Pattern pattern = RegExp(r'^[a-zA-Z]+$');
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(text)) {
      return false;
    } else {
      return true;
    }
  }

  // this function check valid home country licence id
  static bool isValidHomeCountryLicense({required String text}) {
    final pattern = RegExp(r'^[a-zA-Z0-9]+$');
    if (!pattern.hasMatch(text)) {
      return false;
    } else {
      if (text.trim().isNotEmpty) {
        return true;
      } else {
        return false;
      }
    }
  }
}
