import 'package:book_selling_app/features/authorization/data/repositories/UserRepositoryImp.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/GetUserUseCase.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/UpdateLatLon.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/add_user_usecase.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/log_out_usecase.dart';
import 'package:book_selling_app/features/authorization/presentation/providers/VerifyOTPController.dart';
import 'package:book_selling_app/features/authorization/presentation/providers/login_controller.dart';
import 'package:book_selling_app/features/books/data/repositories/book_repository_imp.dart';
import 'package:book_selling_app/features/books/domain/usecases/add_book_usecase.dart';
import 'package:book_selling_app/features/books/presentation/providers/AccountController.dart';
import 'package:book_selling_app/features/books/presentation/providers/AllBookListController.dart';
import 'package:book_selling_app/features/books/presentation/providers/UploadBookController.dart';
import 'package:book_selling_app/features/location/data/repository/PinCodeRepositoryImpl.dart';
import 'package:book_selling_app/features/location/presentation/controller/PickLocationController.dart';
import 'package:get/get.dart';

import '../../features/authorization/data/repositories/LoginRepositoryImp.dart';
import '../../features/books/presentation/providers/HomeController.dart';
import '../../features/other/presentation/controller/SplashController.dart';

class AllBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SplashController());

    Get.lazyPut(() => LoginController(LoginRepositoryImp()));

    Get.lazyPut(() => VerifyOTPController(LoginRepositoryImp()));

    Get.lazyPut(() => HomeController(
        AddUser(UserRepositoryImp()),
        GetUser(UserRepositoryImp()),
        BookRepositoryImp(),
        LocalityRepositoryImpl(),
        UpdateLatLon(UserRepositoryImp())));

    Get.lazyPut(() => UploadBookController(
        AddUser(UserRepositoryImp()), AddBook(BookRepositoryImp())));

    Get.lazyPut(() => AccountController(
        GetUser(UserRepositoryImp()), LogOutUseCase(UserRepositoryImp())));

    Get.lazyPut(() => AllBookListController());

  }

}
