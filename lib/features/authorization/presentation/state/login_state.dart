import 'package:flutter/material.dart';

class LoginState {
  final bool isSuccess;
  final bool isLoading;
  final String? errorMessage;
  final bool isLoginEnable;
  final TextEditingController usernameController;
  final TextEditingController userPhoneController;

  LoginState(
    this.isSuccess,
    this.isLoading,
    this.errorMessage,
    this.isLoginEnable,
    this.usernameController,
    this.userPhoneController,
  );

  factory LoginState.empty() {
    return LoginState(
      false,
      false,
      null,
      true,
      TextEditingController(),
      TextEditingController(),
    );
  }

  LoginState copyWith({
    bool? isSuccess,
    bool? isLoading,
    String? errorMessage,
    bool? isLoginEnable,
    String? username,
    String? userPhone,
  }) {
    if (username != null) {
      usernameController.text = username;
    }
    if (userPhone != null) {
      userPhoneController.text = userPhone;
    }
    return LoginState(
        isSuccess ?? this.isSuccess,
        isLoading ?? this.isLoading,
        errorMessage ?? this.errorMessage,
        isLoginEnable ?? this.isLoginEnable,
        usernameController,
        userPhoneController);
  }
}
