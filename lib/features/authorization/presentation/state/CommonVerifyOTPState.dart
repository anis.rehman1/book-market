import 'dart:async';

class VerifyOTPState {
  final bool isSuccess;
  final bool isLoading;
  final String? errorMessage;
  final int? hour;
  final int? seconds;
  final int? minutes;
  final bool? isResendEnabled;
  final Timer? countdownTimer;


  VerifyOTPState(
    this.isSuccess,
    this.isLoading,
    this.errorMessage,
    this.hour,
    this.minutes,
    this.seconds,
    this.isResendEnabled,
      this.countdownTimer,
  );

  factory VerifyOTPState.empty() {
    return VerifyOTPState(false, false, null, 0, 0, 0, false, null);
  }

  VerifyOTPState copyWith({
    bool? isSuccess,
    bool? isLoading,
    String? errorMessage,
    int? hour,
    int? minutes,
    int? second,
    bool? isResendEnabled,
    Timer? countdownTimer,
  }) {
    return VerifyOTPState(
      isSuccess ?? this.isSuccess,
      isLoading ?? this.isLoading,
      errorMessage ?? this.errorMessage,
      hour ?? this.hour,
      minutes ?? this.minutes,
      second ?? seconds,
      isResendEnabled ?? this.isResendEnabled,
        countdownTimer ?? this.countdownTimer
    );
  }
}
