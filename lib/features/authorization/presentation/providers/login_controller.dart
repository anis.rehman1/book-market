
import 'package:book_selling_app/config/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../data/repositories/LoginRepositoryImp.dart';


class LoginController extends GetxController {

  final LoginRepositoryImp loginRepositoryImp;

  LoginController(this.loginRepositoryImp);

  RxBool isLoading = false.obs;
  RxBool otpSent = false.obs;
  RxString errorMessage = "".obs;
  RxBool isButtonEnabled = false.obs;
  late TextEditingController userNameController;
  late TextEditingController phoneController;

  Future<void> getOTP({ required BuildContext context}) async {
    isLoading.value = true;
    print(userNameController.text);
    print(phoneController.text);
    otpSent.value = await loginRepositoryImp.sendOTP("91${phoneController.text}");
    otpSent.value ?
    Get.offNamed(AppRoutes.verifyOtp, arguments: [
        {"username": userNameController.text},
        {"phone": phoneController.text}
        ])
    : setErrorMessage();
    isLoading.value = false;
  }

  bool validateNumber() {
    print("validating number");
    return phoneController.text.length == 10;
  }

  bool validateName() {
    return userNameController.text.length > 2;
  }

  setErrorMessage() {
    errorMessage.value = "Error occurred / OTP not sent";
  }

  @override
  void onInit() {
    super.onInit();
    userNameController = TextEditingController();
    phoneController = TextEditingController();
  }

  void enableLoginButton() {
      var isEnabled = validateName() && validateNumber();
      isButtonEnabled.value = isEnabled;
  }
}
