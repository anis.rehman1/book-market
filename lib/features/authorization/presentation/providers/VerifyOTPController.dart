
import 'dart:async';

import 'package:get/get.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../../../config/constants.dart';
import '../../data/repositories/LoginRepositoryImp.dart';


class VerifyOTPController extends GetxController {

  RxBool isLoading = false.obs;
  RxInt sec = 00.obs;
  RxInt min = 00.obs;
  RxBool resendEnabled = false.obs;
  final LoginRepositoryImp loginRepositoryImp;

  VerifyOTPController(this.loginRepositoryImp);

  @override
  void onInit() {
    super.onInit();
    updateTime();
  }

  void updateTime() {
    Duration myDuration = const Duration(minutes: 1);
    const reduceSecondsBy = 1;
    Timer? countdownTimer;
    countdownTimer = Timer.periodic(const Duration(seconds: 1), (_) {
      sec.value = myDuration.inSeconds.remainder(60);
      min.value = myDuration.inMinutes.remainder(60);

      final seconds = myDuration.inSeconds - reduceSecondsBy;
      if (seconds < 0) {
        resendEnabled.value = true;
        countdownTimer!.cancel();
      } else {
        myDuration = Duration(seconds: seconds);
      }
    });
  }

  Future<void> verifyOTP(String otp, String number, Function(AuthResponse?) onComplete) async {
    isLoading.value = true;
    print(otp);
    print(number);
    try {
      final AuthResponse res = await supabase.auth.verifyOTP(
        type: OtpType.sms,
        token: otp,
        phone: number,
      );
      onComplete(res);
      isLoading.value = false;
    }
    catch(e){
      isLoading.value = false;
      print(e.toString());
    }
  }

  Future<void> resendOTP(String number) async {
    isLoading.value = true;
    print("Resending OTP to $number");
    try {
      await loginRepositoryImp.sendOTP(number);
      isLoading.value = false;
    }
    catch(e){
      isLoading.value = false;
      print(e.toString());
    }
  }
}
