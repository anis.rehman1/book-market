
import 'package:book_selling_app/config/images/ImageName.dart';
import 'package:book_selling_app/config/util/PreferenceKeys.dart';
import 'package:book_selling_app/config/util/SharedPreferenceUtil.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/GetUserUseCase.dart';
import 'package:book_selling_app/features/authorization/presentation/providers/VerifyOTPController.dart';
import 'package:book_selling_app/features/location/data/repository/PinCodeRepositoryImpl.dart';
import 'package:book_selling_app/features/other/presentation/controller/SplashController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../config/constants.dart';
import '../../../../config/images/GetImage.dart';
import '../../../../config/routes/app_routes.dart';
import '../../../../config/ui_components/CommonOTPField.dart';
import '../../../../config/ui_components/CommonProgressBar.dart';
import '../../../../config/ui_components/CommonText.dart';
import '../../../../config/ui_components/FontsConstant.dart';
import '../../../../config/ui_components/decoration.dart';
import '../../../../config/ui_components/theme_data.dart';
import '../../../books/data/repositories/book_repository_imp.dart';
import '../../../books/presentation/providers/HomeController.dart';
import '../../data/repositories/UserRepositoryImp.dart';
import '../../domain/usecases/UpdateLatLon.dart';
import '../../domain/usecases/add_user_usecase.dart';

class VerifyOTPScreen extends StatelessWidget {
  const VerifyOTPScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double wid = MediaQuery.of(context).size.width;
    double hgt = MediaQuery.of(context).size.height;

    return Scaffold(
        body: Stack(children: [
          Positioned(
            top: -getDeviceWidth() * .45,
            // Adjust position to show part of the circle
            right: -getDeviceWidth() * .45,
            // Adjust right position
            child: Container(
              width: getDeviceWidth() * .8,
              // Make circle slightly bigger for better effect
              height: getDeviceWidth()* .8,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: accentColor, // Change color as desired
              ),
            ),
          ),

          // Bottom Left Circle
          Positioned(
              top: getDeviceHeight() * .10,
              right: getDeviceWidth() * .80,
              child: Container(
                width: getDeviceWidth() * .60,
                height: getDeviceWidth() * .60,
                decoration:
                const BoxDecoration(shape: BoxShape.circle, color: secondry),
              )),
      Padding(
        padding: EdgeInsets.fromLTRB(wid * .1, hgt * .10, wid * .1, 0),
        child: GetX<VerifyOTPController>(
          builder: (verifyOTPController) => Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Center(
                child: GetImage.svgFromAssets(splashIcon,
                    wid * .4,
                    wid * .4),
              ),
              sizeBox30,
              const Center(
                child: CommonText(
                  value: "Enter OTP",
                  textColor: textColorBlack,
                  fontSize: 30,
                  fontWeight: FontWeight.w400,
                ),
              ),
              sizeBox20,
              CommonOTPField(
                isError: false,
                onChange: (value) {},
                onComplete: (value) async {
                  verifyOTPController.verifyOTP(
                      value, "91${Get.arguments[1]["phone"]}" ?? "", (auth) {
                    if (auth?.session == null) {
                      print("Session is null Nulllll");
                    } else {
                      print("Session not null");
                      print(auth?.session?.user.toString());
                      Preference.setString(PreferenceKeys.USER_NAME,
                          Get.arguments[0]["username"]);
                      Get.lazyPut(() => SplashController());
                      Get.lazyPut(() => HomeController(
                          AddUser(UserRepositoryImp()),
                          GetUser(UserRepositoryImp()),
                          BookRepositoryImp(),
                          LocalityRepositoryImpl(),
                          UpdateLatLon(UserRepositoryImp())));

                      Get.offNamed(AppRoutes.splash);
                      print("moving to home");
                    }
                  });
                },
              ),
              Row(
                children: [
                  const CommonText(
                    value: "Not received yet ",
                    fontSize: 12,
                    textColor: disableGrayColor,
                  ),
                  InkWell(
                    onTap: () async {
                      verifyOTPController
                          .resendOTP("91${Get.arguments[1]["phone"]}");
                    },
                    child: verifyOTPController.resendEnabled.value == true
                        ? const CommonText(
                            value: "Resend",
                            textColor: secondary,
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                            fontFamilyName: Medium,
                          )
                        : Container(),
                  ),
                  sizeWidthBox5,
                  CommonText(
                    value:
                        '${verifyOTPController.min.value}:${verifyOTPController.sec.value}',
                  ),
                ],
              ),
              sizeBox10,
              const Center(
                  child: Padding(
                padding: EdgeInsets.all(8.0),
                child: CommonText(
                  value: "",
                  textColor: errorColor,
                ),
              )),
              sizeBox10,
              if (verifyOTPController.isLoading.value)
                Align(
                  alignment: Alignment.bottomCenter,
                  child: CommonProgressBar(
                    colorAndroid: secondary,
                    colorIos: secondary,
                  ),
                ),
            ],
          ),
        ),
      ),
    ]));
  }
}
