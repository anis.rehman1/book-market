import 'package:book_selling_app/config/images/ImageName.dart';
import 'package:book_selling_app/features/authorization/presentation/providers/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../config/constants.dart';
import '../../../../config/images/GetImage.dart';
import '../../../../config/ui_components/CommonButton.dart';
import '../../../../config/ui_components/CommonText.dart';
import '../../../../config/ui_components/CommonTextField.dart';
import '../../../../config/ui_components/decoration.dart';
import '../../../../config/ui_components/loading_overlay.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});

  final FocusNode _userNameNode = FocusNode();
  final FocusNode _userPhoneNumberNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => {},
        child: Scaffold(
          body: GetX<LoginController>(
            builder: (logincontroller) => SingleChildScrollView(
              physics: const AlwaysScrollableScrollPhysics(),
              child: Stack(
                children: [
                  Positioned(
                    top: -getDeviceWidth() * .5,
                    // Adjust position to show part of the circle
                    right: -getDeviceWidth() * .5,
                    // Adjust right position
                    child: Container(
                      width: getDeviceWidth(),
                      // Make circle slightly bigger for better effect
                      height: getDeviceWidth(),
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: accentColor, // Change color as desired
                      ),
                    ),
                  ),

                  // Bottom Left Circle
                  Positioned(
                      top: getDeviceHeight() * .20,
                      right: getDeviceWidth() * .65,
                      child: Container(
                        width: getDeviceWidth() * .65,
                        height: getDeviceWidth() * .65,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: secondry),
                      )),
                  Positioned(
                      top: getDeviceHeight() * .1,
                      right: getDeviceWidth() * .28,
                      child: GetImage.svgFromAssets(splashIcon,
                          getDeviceWidth() * .5, getDeviceWidth() * .5)),
                  Padding(
                    padding: EdgeInsets.only(
                        top: getDeviceHeight() * .5, left: 20, right: 20),
                    child: Column(
                      children: [
                        Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            sizeBox20,
                            const CommonText(
                              value: "Login",
                              textColor: textColorBlack,
                              fontSize: 30,
                              fontWeight: FontWeight.w400,
                            ),

                            sizeBox30,
                            // Spacer(),
                            const CommonText(
                              value: "Your Name",
                              textColor: textColorBlack,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                            CommonTextField(
                              node: _userNameNode,
                              // borderColor: dividerColor,
                              // focusBorderColor: dividerColor,
                              hintLabel: "",
                              labelText: "",
                              contentPadding: const EdgeInsets.all(0),
                              controller: logincontroller.userNameController,
                              onEditComplete: () {},
                              onTextChanges: (value) {
                                logincontroller.enableLoginButton();
                              },
                              onValidate: (value) {},
                              border: const UnderlineInputBorder(
                                borderSide: BorderSide(color: dividerColor),
                              ),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: dividerColor),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: dividerColor),
                              ),
                            ),
                            sizeBox25,
                            //Spacer(),
                            const CommonText(
                              value: "Phone Number",
                              textColor: textColorBlack,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),

                            CommonTextField(
                              labelText: "",
                              textInputType: TextInputType.phone,
                              node: _userPhoneNumberNode,
                              contentPadding: const EdgeInsets.all(0),
                              // borderColor: dividerColor,
                              // focusBorderColor: dividerColor,
                              hintLabel: "",
                              controller: logincontroller.phoneController,
                              onEditComplete: () {},
                              onTextChanges: (value) {
                                logincontroller.enableLoginButton();
                              },
                              onValidate: (value) {},
                              border: const UnderlineInputBorder(
                                borderSide: BorderSide(color: dividerColor),
                              ),
                              enabledBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: dividerColor),
                              ),
                              focusedBorder: const UnderlineInputBorder(
                                borderSide: BorderSide(color: dividerColor),
                              ),
                              maxLength: 10,
                            ),
                            sizeBox15,
                            //  Spacer(),
                            sizeBox30,
                            logincontroller.errorMessage.isNotEmpty
                                ? Center(
                                    child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: CommonText(
                                      value: logincontroller.errorMessage.value,
                                      textColor: errorColor,
                                      fontSize: 12,
                                    ),
                                  ))
                                : Container(),
                            //Spacer(),
                            sizeBox10,
                            Padding(
                              padding: avgPaddingHorizontal,
                              child: Center(
                                child: SizedBox(
                                    width: double.infinity,
                                    // height: 48,
                                    child: CommonButton(
                                      isEnabled:
                                          logincontroller.isButtonEnabled.value,
                                      text: "Verify Phone",
                                      onClick: () async {
                                        if (logincontroller
                                            .isButtonEnabled.value) {
                                          logincontroller.getOTP(
                                              context: context);
                                        }
                                      },
                                    )),
                              ),
                            ),
                            //Spacer(),
                          ],
                        ),
                      ],
                    ),
                  ),
                  if (logincontroller.isLoading.value) const LoadingOverlay()
                ],
              ),
            ),
          ),
        ));
  }
}
