
import 'package:book_selling_app/features/authorization/domain/repositories/login_repository.dart';

import '../../../../config/constants.dart';

class LoginRepositoryImp extends LoginRepository{
  @override
  Future<bool> sendOTP(String phone) async {
    try {
      await supabase.auth.signInWithOtp(
        phone: phone,
      );
      return true;
    }
    catch(e){
      return false;
    }
  }

}