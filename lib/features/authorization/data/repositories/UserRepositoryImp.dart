import 'package:book_selling_app/features/authorization/domain/entities/AddressData.dart';
import 'package:book_selling_app/features/authorization/domain/entities/user_entity.dart';
import 'package:book_selling_app/features/authorization/domain/repositories/user_repository.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../config/constants.dart';
import '../../../../config/util/PreferenceKeys.dart';
import '../../../../config/util/SharedPreferenceUtil.dart';

class UserRepositoryImp extends UserRepository{

  final String USER_TABLE = 'User';
  @override
  Future<UserModel> addUser(LatLng position, Placemark placemark) async {
    var userName = Preference.getString(PreferenceKeys.USER_NAME);
    return await supabase
        .from(USER_TABLE)
        .upsert(
        {
          'name': userName,
          'email': '',
          'latitude': position.latitude,
          'longitude': position.longitude,
          'address': "${placemark.street}, ${placemark.subLocality}, ${placemark.locality}",
          'phone': supabase.auth.currentUser?.phone,
          'user_id': supabase.auth.currentUser?.id,
        });
  }

  @override
  Future<dynamic> getUser() async {
    print(">>>>>> in get user");
    var data =  await supabase
        .from(USER_TABLE).
    select('*').filter("user_id", "eq", supabase.auth.currentUser?.id);

      if(data.isEmpty) {
        return null;
      }
     return UserModel(
        data.first["user_id"],
        data.first["created_at"],
        data.first["name"] ?? "",
        data.first["email"],
        data.first["latitude"],
        data.first["longitude"],
        data.first["address"],
      );
  }

  @override
  Future<void> logOut() async {
    await supabase.auth.signOut();
  }

  @override
  Future<bool> updateLatLon(AddressData addressData) async {
    await supabase
        .from(USER_TABLE)
        .update({
      "latitude": addressData.latitude,
      "longitude": addressData.longitude,
      "address": addressData.address,
    }).match({'user_id': supabase.auth.currentUser?.id ?? ""}).then((value){
      print("responseeee = $value");
    }); // Update the row with the specified ID

    return Future.value(true);
  }
}