
class AddressData {
  final double? latitude;
  final double? longitude;
  final String? address;

  AddressData(
      this.latitude,
      this.longitude,
      this.address,
      );
}
