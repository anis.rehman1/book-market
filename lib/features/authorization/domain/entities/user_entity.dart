

class UserModel {
  final String userID;
  final String? userName;
  final String phone;
  final String? email;
  final double? latitude;
  final double? longitude;
  final String? address;

  UserModel(
      this.userID,
      this.userName,
      this.phone,
      this.email,
      this.latitude,
      this.longitude,
      this.address,
      );
}
