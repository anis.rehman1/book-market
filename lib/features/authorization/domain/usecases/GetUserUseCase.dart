import 'package:book_selling_app/features/authorization/domain/entities/user_entity.dart';
import 'package:book_selling_app/features/authorization/domain/repositories/user_repository.dart';


class GetUser {

  final UserRepository userRepository;

  GetUser(this.userRepository);

  Future<dynamic> get(){
    return userRepository.getUser();
  }
}