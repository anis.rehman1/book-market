import 'package:book_selling_app/features/authorization/domain/repositories/user_repository.dart';


class LogOutUseCase {

  final UserRepository userRepository;

  LogOutUseCase(this.userRepository);

  Future<void> call(){
    return userRepository.logOut();
  }
}