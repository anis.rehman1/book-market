import 'package:book_selling_app/features/authorization/domain/entities/AddressData.dart';
import 'package:book_selling_app/features/authorization/domain/entities/user_entity.dart';
import 'package:book_selling_app/features/authorization/domain/repositories/user_repository.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class UpdateLatLon {

  final UserRepository userRepository;

  UpdateLatLon(this.userRepository);

  Future<bool> update(AddressData addressData){
    return userRepository.updateLatLon(addressData);
  }
}