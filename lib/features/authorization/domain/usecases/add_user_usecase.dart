import 'package:book_selling_app/features/authorization/domain/entities/user_entity.dart';
import 'package:book_selling_app/features/authorization/domain/repositories/user_repository.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AddUser {

  final UserRepository userRepository;

  AddUser(this.userRepository);

  Future<UserModel> callAddUser(LatLng position, Placemark placemark){
    return userRepository.addUser(position, placemark);
  }
}