//
// import 'package:hooks_riverpod/hooks_riverpod.dart';
//
// var verifyOTPRepositoryProvider =
//     Provider((ref) => VerifyOTPRepositoryImp(ref.read(apiHelperProvider)));
//
// abstract class VerifyOTPRepository {
//   Future<Either<Exception, VerifyOTPResponse>> verify(VerifyOTPModel params);
// }
//
// class VerifyOTPRepositoryImp implements VerifyOTPRepository {
//   final ApiHelper apiHelper;
//
//   VerifyOTPRepositoryImp(this.apiHelper);
//
//   @override
//   Future<Either<Exception, VerifyOTPResponse>> verify(VerifyOTPModel params) {
//     return apiHelper.postRequest(
//       path: pathVerifyOTP,
//       body: params.toJson(),
//       headers: headerMap,
//       create: () => VerifyOTPResponse(),
//     );
//   }
// }
