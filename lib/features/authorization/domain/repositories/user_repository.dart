import 'package:book_selling_app/features/authorization/domain/entities/AddressData.dart';
import 'package:geocoding_platform_interface/src/models/placemark.dart';
import 'package:geolocator_platform_interface/src/models/position.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../entities/user_entity.dart';

abstract class UserRepository{
    Future<UserModel> addUser(LatLng position, Placemark first);
    Future<dynamic> getUser();
    Future<void> logOut();
    Future<bool> updateLatLon(AddressData addressData);
}