import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/config/ui_components/CommonButton.dart';
import 'package:book_selling_app/config/ui_components/CommonText.dart';
import 'package:book_selling_app/features/location/data/repository/PinCodeRepositoryImpl.dart';
import 'package:book_selling_app/features/location/presentation/controller/PickLocationController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class PickLocationScreen extends StatelessWidget {
  const PickLocationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.lazyPut(() => PickLocationController(LocalityRepositoryImpl()));
    var pickLocationController = Get.find<PickLocationController>();
    return GestureDetector(
        onTap: () => {},
        child: Scaffold(
          appBar: AppBar(title: const Text('Pick Location')),
          body: Stack(children: [
            Positioned(
              width: getDeviceWidth(),
              height: getDeviceHeight() * .1,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: SizedBox(
                  width: getDeviceWidth(),
                  height: getDeviceHeight() * .1,
                  child: TextFormField(
                    controller:
                        pickLocationController.locationEnterTextController,
                    decoration:
                        const InputDecoration(hintText: "Enter Address"),
                  ),
                ),
              ),
            ),
            Positioned(
              top: getDeviceHeight() * .07,
              right: 0,
              width: getDeviceWidth(),
              height: getDeviceHeight(),
              child: GetX<PickLocationController>(
                builder: (controller) =>  Stack(children: [
                  SizedBox(
                    width: getDeviceWidth(),
                    height: getDeviceHeight() * .8,
                    child: GoogleMap(
                      mapType: MapType.terrain,
                      markers: {pickLocationController.marker.value}.toSet(),
                      initialCameraPosition: pickLocationController.initialPosition,
                      onMapCreated: (GoogleMapController controller) {
                        pickLocationController.googleMapController.complete(controller);
                      },
                    ),
                  ),
                  Padding(
                      padding:
                          const EdgeInsets.symmetric(vertical: 0, horizontal: 16),
                      child: Container(
                        color: Colors.white,
                        child: ListView.builder(
                            itemCount: pickLocationController.placesList.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: InkWell(
                                  onTap: () async {
                                    pickLocationController.geocode(
                                        pickLocationController.placesList[index]
                                            ["description"]);
                                  },
                                  child: CommonText(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                    value: pickLocationController
                                        .placesList[index]["description"],
                                  ),
                                ),
                              );
                            }),
                      ),
                    ),
                ]),
              ),
            ),
            Positioned(
              top: getDeviceHeight() * .75,
                child: SizedBox(
                  width: getDeviceWidth() ,
                  child: Center(
                    child: SizedBox(
                      width: getDeviceWidth() * .8,
                      child: CommonButton(
                        onClick: () { 
                            Get.back(result: pickLocationController.addressData);
                        },
                        text: 'Set this location',
                      ),
                    ),
                  ),
                ))
          ]),
        ));
  }
}
