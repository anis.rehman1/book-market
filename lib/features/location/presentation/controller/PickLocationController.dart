import 'dart:async';
import 'dart:convert';

import 'package:book_selling_app/config/ui_components/decoration.dart';
import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:book_selling_app/features/authorization/domain/entities/AddressData.dart';
import 'package:book_selling_app/features/books/presentation/providers/HomeController.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:uuid/uuid.dart';
import 'package:http/http.dart' as http;

import '../../data/repository/PinCodeRepositoryImpl.dart';

class PickLocationController extends GetxController {
  final Completer<GoogleMapController> googleMapController =
      Completer<GoogleMapController>();

  TextEditingController locationEnterTextController = TextEditingController();
  LocalityRepositoryImpl localityRepositoryImp;

  PickLocationController(this.localityRepositoryImp);

  CameraPosition initialPosition = const CameraPosition(
    target: LatLng(26.90164953264282, 75.75895782851333),
    zoom: 14.4746,
  );

  late Rx<Marker> marker;
  late AddressData addressData;
  Uuid uuid = const Uuid();
  String _sessionToken = "";
  RxList<dynamic> placesList = RxList();

  @override
  void onInit() {
    super.onInit();
    print("initialized Pick location controller");
    marker = Rx(const Marker(
        markerId: MarkerId("1"),
        position: LatLng(26.90164953264282, 75.75895782851333)));
    locationEnterTextController.addListener(() {
      onChange();
    });
  }

  List<String> getBookCategoryText() {
    var bookCategories = Get.find<HomeController>().bookCategories;
    List<String> list = [];
    for (var items in bookCategories) {
      list.add(items.name ?? "");
    }
    return list;
  }

  void onChange() {
    if (_sessionToken.isEmpty) {
      _sessionToken = uuid.v4();
    }
    if (locationEnterTextController.text.length > 2) {
      getSuggestion();
    }
  }

  Future<void> getSuggestion() async {
    String placesApiKey = "AIzaSyDWrWBdA56kBRAfa2I9Uo5fDuSe7Gu9EWs";
    String baseURL =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json';
    String request =
        '$baseURL?input=${locationEnterTextController.text}&key=$placesApiKey&sessiontoken=$_sessionToken';

    var response = await http.get(Uri.parse(request));
    if (response.isBlank ?? true) {
      print("Empty response");
    } else {
      print(response.body);
      placesList.value = jsonDecode(response.body.toString())["predictions"];
    }
  }

  Future<void> geocode(String address) async {
    placesList.value = [];
    print("address $address");
    var locationList = await locationFromAddress(address);
    addressData = AddressData(locationList.last.latitude, locationList.last.longitude, address);

    print("lat ${locationList.last.latitude}");
    print("lon ${locationList.last.longitude}");
    List<Placemark> placemarks = await placemarkFromCoordinates(locationList.last.latitude, locationList.last.longitude);
    if(placemarks.isEmpty){
      Get.snackbar(
        "Error",
        "This service service is not available at the selected area.",
        colorText: white,
        backgroundColor: Colors.redAccent,
        icon: const Icon(Icons.info_outline),
      );
      return;
    }

    print("lat ${locationList.last.latitude}");
    print("lon ${locationList.last.longitude}");
    print("placemark ${placemarks.first}");

    bool availableAtLocality = await localityRepositoryImp.isAvailableAtLocality(placemarks.first.locality ?? "");

    if(!availableAtLocality){
      Get.snackbar(
        "Please select other location",
        "This service service is not available at the selected area.",
        colorText: white,
        backgroundColor: secondary,
        icon: const Icon(Icons.info_outline),
      );
      return;
    }

    marker.value = Marker(
        markerId: const MarkerId("2"),
        position:
            LatLng(locationList.last.latitude, locationList.last.longitude));
    CameraPosition cameraPosition = CameraPosition(
        zoom: 14,
        target:
            LatLng(locationList.last.latitude, locationList.last.longitude));
    var controller = await googleMapController.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
  }

}
