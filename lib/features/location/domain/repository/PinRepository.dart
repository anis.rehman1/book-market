
abstract class LocalityRepository{
  Future<bool> isAvailableAtLocality(String pinCode);
}