import 'package:book_selling_app/features/location/domain/repository/PinRepository.dart';

import '../../../../config/constants.dart';

class LocalityRepositoryImpl extends LocalityRepository {
  @override
  Future<bool> isAvailableAtLocality(String pinCode) async {
    print("pincode fetching $pinCode");
    final result = await supabase
        .from('Locality') // Replace with your table name
        .select('id') // Select only the 'id' column for efficiency
        .eq('locality', pinCode);
    print("locality found ${result.isNotEmpty}");
    return result.isNotEmpty;
  }

}