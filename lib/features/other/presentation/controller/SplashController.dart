import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/config/routes/app_routes.dart';
import 'package:book_selling_app/features/books/presentation/providers/HomeController.dart';
import 'package:get/get.dart';

class SplashController extends GetxController{

  @override
  void onInit() {
    super.onInit();
    // Simulate a delay for splash screen
    Get.find<HomeController>();
    Future.delayed(const Duration(seconds: 3), () {
      // Navigate to the next screen after the delay
      supabase.auth.currentSession == null ? Get.offNamed(AppRoutes.login) :
      Get.offNamed(AppRoutes.home);
    });
  }

  void postData() {}

}