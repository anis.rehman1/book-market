import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/config/images/GetImage.dart';
import 'package:book_selling_app/config/images/ImageName.dart';
import 'package:book_selling_app/config/ui_components/decoration.dart';
import 'package:book_selling_app/features/other/presentation/controller/SplashController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.find<SplashController>();
    deviceWidth = MediaQuery.of(context).size.width;
    deviceHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Stack(
        children: [
          // Centered Text
          Center(
            child: GetImage.svgFromAssets(
                splashIcon, getDeviceWidth() * .7, getDeviceWidth() * .7),
          ),

          // Top Right Circle
          Positioned(
            top: -getDeviceWidth() * .5,
            // Adjust position to show part of the circle
            right: -getDeviceWidth() * .5,
            // Adjust right position
            child: Container(
              width: getDeviceWidth(),
              // Make circle slightly bigger for better effect
              height: getDeviceWidth(),
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: accentColor, // Change color as desired
              ),
            ),
          ),

          // Bottom Left Circle
          Positioned(
              top: getDeviceHeight() - (getDeviceWidth() * .5),
              right: getDeviceWidth() * .5,
              child: Container(
                width: getDeviceWidth(),
                height: getDeviceWidth(),
                decoration: const BoxDecoration(
                    shape: BoxShape.circle, color: secondry),
              ))
        ],
      ),
    );
  }
}
