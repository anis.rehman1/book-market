
import 'package:book_selling_app/features/books/domain/entities/Book.dart';
import 'package:book_selling_app/features/books/domain/repositories/book_repository.dart';

class AddBook {

  final BookRepository bookRepository;

  AddBook(this.bookRepository);

  Future<Null> call(Book book){
    return bookRepository.addBook(book);
  }
}