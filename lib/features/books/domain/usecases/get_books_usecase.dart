import 'package:book_selling_app/features/books/domain/entities/Book.dart';
import 'package:book_selling_app/features/books/domain/repositories/book_repository.dart';

class GetBooks {

  final BookRepository bookRepository;

  GetBooks(this.bookRepository);

  Future<List<Book>> call(){
    return bookRepository.fetchBooksAsPerLocality();
  }
}