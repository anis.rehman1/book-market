
import 'package:book_selling_app/features/books/domain/entities/Book.dart';
import 'package:book_selling_app/features/books/domain/entities/BookCategory.dart';

abstract class BookRepository {
  Future<List<Book>> fetchBooksAsPerLocality();
  Future<Null> addBook(Book book);
  Future<List<BookCategory>> getBookCategory();
  Future<List<BookType>> getBookType();
}