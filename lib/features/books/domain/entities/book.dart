
class Book {
  final int? id;
  final String? created_at;
  final String? name;
  final String? author;
  final String? description;
  final int? pages;
  final double? rating;
  final int? category;
  final String? price;
  final String? uploader_id;
  final Map<String, dynamic>? images;
  final int? type;
  final String? locality;

  Book(
      this.id,
      this.created_at,
      this.name,
      this.author,
      this.description,
      this.pages,
      this.rating,
      this.category,
      this.price,
      this.uploader_id,
      this.images,
      this.type,
      this.locality,
      );
}
