
class BookCategory {
  final int? id;
  final String? created_at;
  final String? name;

  BookCategory(
      this.id,
      this.created_at,
      this.name,
      );
}


class BookType {
  final int? id;
  final String? created_at;
  final String? name;

  BookType(
      this.id,
      this.created_at,
      this.name,
      );
}
