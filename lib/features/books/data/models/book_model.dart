
class BookModel {
  final int bookId;
  final String title;
  final String author;
  final int categoryId;
  final int year;
  final int status;
  final int pages;
  final double price;

  BookModel(
      this.bookId,
      this.title,
      this.author,
      this.categoryId,
      this.year,
      this.status,
      this.pages,
      this.price,
      );

}
