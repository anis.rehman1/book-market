import 'dart:math';

import 'package:book_selling_app/features/books/domain/entities/Book.dart';
import 'package:book_selling_app/features/books/domain/entities/BookCategory.dart';
import 'package:book_selling_app/features/books/domain/repositories/book_repository.dart';

import '../../../../config/constants.dart';

class BookRepositoryImp extends BookRepository {
  @override
  Future<Null> addBook(Book book) async {
    int randomNumber = Random().nextInt(9000000) + 1000000;
     return await supabase
        .from('Book')
        .insert(
        {
          'id': randomNumber,
          'name': book.name,
          'author': book.author,
          'description': book.description,
          'pages': book.pages,
          'category': book.category,
          'price': book.price,
          'uploader_id': book.uploader_id,
          'images': book.images,
          'type': book.type,
          'rating': book.rating,
          'locality': book.locality,
        });
     // throw Exception();
  }

  @override
  Future<List<Book>> fetchBooksAsPerLocality()  async {
    var data = await supabase
        .from("Book")
        .select('*')
        .eq("locality", userAddress?.locality ?? "");

    List<Book> booklist = [];
     for(int i = 0; i < data.length; i++){
       Book book = Book(
         data[i]["id"],
         data[i]["created_at"],
         data[i]["name"],
         data[i]["author"],
         data[i]["description"],
         data[i]["pages"],
         data[i]["rating"],
         data[i]["category"],
         data[i]["price"],
         data[i]["uploader_id"],
         data[i]["images"],
         data[i]["type"],
         data[i]["locality"]
       );
       booklist.add(book);
     }
    print("in fetch books >>>${booklist.length}");

    return booklist;
  }

  @override
  Future<List<BookCategory>> getBookCategory() async {
    var data =  await supabase
        .from("Category").
    select('*');
    List<BookCategory> bookCategories = [];
    for(int i = 0; i < data.length; i++){
      print(data[i].toString());
      BookCategory book = BookCategory(
        data[i]["id"],
        data[i]["created_at"],
        data[i]["name"]
      );
      bookCategories.add(book);
    }
    return bookCategories;
  }

  @override
  Future<List<BookType>> getBookType() async {
    var data =  await supabase
        .from("BookType").
    select('*');
    List<BookType> bookType = [];
    for(int i = 0; i < data.length; i++){
      print(data[i].toString());
      BookType book = BookType(
          data[i]["id"],
          data[i]["created_at"],
          data[i]["name"]
      );
      bookType.add(book);
    }
    return bookType;
  }

}
