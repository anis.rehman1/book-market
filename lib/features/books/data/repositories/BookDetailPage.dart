import 'package:book_selling_app/config/ui_components/CommonButton.dart';
import 'package:book_selling_app/config/ui_components/decoration.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../../../config/images/GetImage.dart';
import '../../../../config/ui_components/CommonText.dart';
import '../../domain/entities/Book.dart';

class BookDetailScreen extends StatelessWidget {
  const BookDetailScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var book = Get.arguments as Book;
    var size = MediaQuery.of(context).size;
    double width = size.width;
    double height = size.height;
    var image = book.images?['image1'] ?? "";
    List<String> imageList = [
      book.images?['image1'] ?? "",
      book.images?['image2'] ?? "",
      book.images?['image3'] ?? ""
    ];
    return GestureDetector(
        onTap: () => {},
        child: Scaffold(
          appBar: AppBar(title: Text(book.name ?? "")),
          body: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 20,
                  vertical: 0,
                ),
                child: Column(
                  children: [
                    Expanded(
                      child: Center(
                        child: Column(
                          children: [
                            CarouselSlider(
                              options: CarouselOptions(
                                  autoPlay: true,
                                  enlargeCenterPage: true,
                                  aspectRatio: 2.0,
                                  onPageChanged: (index, reason) {}),
                              items: imageList
                                  .map((item) => Container(
                                        child: Center(
                                            child:
                                                GetImage.cachedImageFromNetwork(
                                          image,
                                          width * .55,
                                          height * .37,
                                        )),
                                      ))
                                  .toList(),
                            ),
                            sizeBox10,
                            CommonText(
                              value: book.name ?? "",
                              fontWeight: FontWeight.w600,
                              fontSize: 24,
                              fontFamilyName: "Poppins",
                            ),
                            CommonText(
                              value: book.author ?? "",
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              fontFamilyName: "Poppins",
                            ),
                            sizeBox10,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Icon(
                                  Icons.star,
                                  size: 36,
                                  color: Colors.yellow,
                                ),
                                CommonText(
                                  value: book.rating.toString() ?? "",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  fontFamilyName: "Poppins",
                                ),
                                CommonText(
                                  value: "INR ${book.price}",
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  fontFamilyName: "Poppins",
                                ),
                              ],
                            ),
                            sizeBox10,
                            const CommonText(
                              value: "Description",
                              fontWeight: FontWeight.w600,
                              fontSize: 20,
                              fontFamilyName: "Poppins",
                            ),
                            const SizedBox(height: 20),
                            CommonText(
                              value: book.description ?? "",
                              fontWeight: FontWeight.w400,
                              textAlign: TextAlign.left,
                              fontSize: 14,
                              fontFamilyName: "Poppins",
                            ),
                            const Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 50.0),
                              child: CommonButton(
                                isEnabled: true,
                                text: "Buy Now At INR ${book.price}",
                                onClick: () async {
                                  _launchUrl();
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}

Future<void> _launchUrl() async {
  final Uri _url = Uri.parse(
      'upi://pay?pa=zunnoon112@ybl&pn=Anis&am=1&tn=Test Payment&cu=INR');

  if (!await launchUrl(_url)) {
    throw Exception('Could not launch $_url');
  }
}
