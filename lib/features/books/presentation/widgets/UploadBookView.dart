import 'dart:io';

import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/config/ui_components/CommonButton.dart';
import 'package:book_selling_app/config/ui_components/CommonText.dart';
import 'package:book_selling_app/config/ui_components/decoration.dart';
import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:book_selling_app/features/books/presentation/providers/UploadBookController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../../../config/ui_components/CommonTextField.dart';


final ImagePicker _picker = ImagePicker();
final TextEditingController maxWidthController = TextEditingController();
final TextEditingController maxHeightController = TextEditingController();
final TextEditingController qualityController = TextEditingController();
final TextEditingController limitController = TextEditingController();

typedef OnPickImageCallback = void Function(
    double? maxWidth, double? maxHeight, int? quality, int? limit);

UploadBookView(){
  var uploadController = Get.find<UploadBookController>();

  return  SingleChildScrollView(
    child: GetX<UploadBookController>(
      builder: (UploadBookController controller) =>
      Column(
        children: [
          sizeBox15,
          const CommonText(value: "Upload Book", textColor: black, fontSize: 18, fontWeight: FontWeight.w600),
          sizeBox10,
          CommonTextField(
            labelText: "Book Name",
            textInputType: TextInputType.text,
            // node: _passwordNode,
            borderColor: dividerColor ,
            focusBorderColor:  dividerColor,
            hintLabel: true
                ? "Book Name"
                : "",
            controller: uploadController.bookNameController,
            onEditComplete: () {

            },
            onTextChanges: (value) {
              controller.enableButton();
            },
            onValidate: (value) {},

          ),
          sizeBox15,
          CommonTextField(
            labelText: "Author",
            textInputType: TextInputType.text,
            // node: _passwordNode,
            borderColor: dividerColor ,
            focusBorderColor:  dividerColor,
            hintLabel: true
                ? "Author"
                : "",
            controller: uploadController.authorController,
            onEditComplete: () {

            },
            onTextChanges: (value) {
              controller.enableButton();
            },
            onValidate: (value) {},

          ),
          sizeBox15,
          CommonTextField(
            labelText: "Description",
            textInputType: TextInputType.text,
            // node: _passwordNode,
            borderColor: dividerColor ,
            focusBorderColor:  dividerColor,
            hintLabel: true
                ? "Description"
                : "",
            controller: uploadController.descriptionController,
            onEditComplete: () {

            },
            onTextChanges: (value) {
              controller.enableButton();
            },
            onValidate: (value) {},

          ),
          sizeBox10,
          CommonTextField(
            labelText: "Price",
            textInputType: TextInputType.text,
            // node: _passwordNode,
            borderColor: dividerColor ,
            focusBorderColor:  dividerColor,
            hintLabel: true
                ? "Price"
                : "",
            controller: uploadController.priceController,
            onEditComplete: () {

            },
            onTextChanges: (value) {
              controller.enableButton();
            },
            onValidate: (value) {},

          ),
        DropdownButton<String>(
          value: uploadController.selectedCategory.value.isEmpty ? null :
          uploadController.selectedCategory.value,
          hint: const Text('Select Category'),
          icon: const Icon(Icons.arrow_drop_down),
          elevation: 16,
          style: const TextStyle(color: black),
          underline: Container(
            height: 1,
            color: secondary,
          ),
          onChanged: (String? value) {
            controller.selectedCategory.value = value ?? "";
          },
          items: uploadController.getBookCategoryText().map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(value),
            );
          }).toList(),
        ),
          sizeBox10,
          const CommonText(value: "Upload Cover Page, Last Page and Middle Page Image of the book."),
          sizeBox10,
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: (){
                    showDialog(
                        context: buildContext! ,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text("Pick"),
                            content: Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  sizeBox20,
                                  InkWell(child: const Text("Camera"), onTap: (){
                                    Navigator.pop(context);
                                    onImageButtonPressed(ImageSource.camera, uploadController.image1);
                                  },),
                                  sizeBox20,
                                  InkWell(child: const Text("Gallery"), onTap: (){
                                    Navigator.pop(context);
                                    onImageButtonPressed(ImageSource.gallery, uploadController.image1);
                                  },),
                                  sizeBox20
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Column(
                    children: [
                      const Icon(
                        Icons.upload_file,
                        size: 80,
                      ),
                      controller.image1.value.isNotEmpty ? const Icon(
                        Icons.file_download_done,
                        size: 25,
                      ):Container(),
                    ],
                  ),
                ),
                sizeBox15,
                InkWell(
                  onTap: (){
                    showDialog(
                        context: buildContext! ,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text("Pick"),
                            content: Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  sizeBox20,
                                  InkWell(child: const Text("Camera"), onTap: (){
                                    Navigator.pop(context);
                                    onImageButtonPressed(ImageSource.camera, uploadController.image2);
                                  },),
                                  sizeBox20,
                                  InkWell(child: const Text("Gallery"), onTap: (){
                                    Navigator.pop(context);
                                    onImageButtonPressed(ImageSource.gallery, uploadController.image2);
                                  },),
                                  sizeBox20
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Column(
                    children: [
                      const Icon(
                        Icons.upload_file,
                        size: 80,
                      ),
                      controller.image2.value.isNotEmpty ? const Icon(
                        Icons.file_download_done,
                        size: 25,
                      ):Container(),
                    ]),
                ),
                sizeBox15,
                InkWell(
                  onTap: (){
                    showDialog(
                        context: buildContext! ,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text("Pick"),
                            content: Container(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  sizeBox20,
                                  InkWell(child: const Text("Camera"), onTap: (){
                                    Navigator.pop(context);
                                    onImageButtonPressed(ImageSource.camera, uploadController.image3);
                                  },),
                                  sizeBox20,
                                  InkWell(child: const Text("Gallery"), onTap: (){
                                    Navigator.pop(context);
                                    onImageButtonPressed(ImageSource.gallery, uploadController.image3);
                                  },),
                                  sizeBox20
                                ],
                              ),
                            ),
                          );
                        });
                  },
                  child: Column(
                    children: [
                      const Icon(
                        Icons.upload_file,
                        size: 80,
                      ),
                      controller.image3.value.isNotEmpty ? const Icon(
                        Icons.file_download_done,
                        size: 25,
                      ):Container(),
                    ]),
                )
              ],
            ),
          ),
          sizeBox25,
          CommonButton(onClick: (){
            controller.uploadBook();
          },
              isEnabled: controller.isButtonEnabled.value,
              text: "Upload Book")
        ],

      ),
    ),
  );
}

Future<void> onImageButtonPressed(
    ImageSource source, RxString image, {
      bool isMultiImage = false,
      bool isMedia = false,
    }) async {

  if (buildContext!.mounted) {
    if (isMedia) {
      print(">>>>>>>>>>isMedia true");
      await _displayPickImageDialog(buildContext!, false, (double? maxWidth,
          double? maxHeight, int? quality, int? limit) async {
        try {
          final List<XFile> pickedFileList = <XFile>[];
          final XFile? media = await _picker.pickMedia(
            maxWidth: maxWidth,
            maxHeight: maxHeight,
            imageQuality: quality,
          );
          if (media != null) {
            print(">>>>>>>>>>media not null");
            pickedFileList.add(media);
          }
          else{
            print(">>>>>>>>>>media null");
          }
        } catch (e) {
          print(">>>>>>>>>>media exception");
        }
      });
    } else {
      print(">>>>>>>>>>isMedia false");
      await _displayPickImageDialog(buildContext!, false, (double? maxWidth,
          double? maxHeight, int? quality, int? limit) async {
        try {
          print(">>>>>>>>>>in pickedFile ");
          final XFile? pickedFile = await _picker.pickImage(
            source: source,
            maxWidth: maxWidth,
            maxHeight: maxHeight,
            imageQuality: quality,
          );
          print(">>>>>>>>>>pickedFile ${pickedFile?.path}");
          final imageFile = File(pickedFile?.path ?? "");
          var inMilliseconds2 = DateTime.now().toString();
          final String path = await supabase.storage.from('book_images').upload(
            'public/${inMilliseconds2.toString()}.jpg',
            imageFile,
            fileOptions: const FileOptions(cacheControl: '3600', upsert: false),
          );
          image.value = "https://lygmlpqoxuonspxaeoio.supabase.co/storage/v1/object/public/$path";
          print("path after upload $path");
          Get.find<UploadBookController>().enableButton();
        } catch (e) {
          print("error${e.toString()}");
        }
      });
    }
  }
}

Future<void> _displayPickImageDialog(
    BuildContext context, bool isMulti, OnPickImageCallback onPick) async {
  final double? width = maxWidthController.text.isNotEmpty
      ? double.parse(maxWidthController.text)
      : null;
  final double? height = maxHeightController.text.isNotEmpty
      ? double.parse(maxHeightController.text)
      : null;
  final int? quality = qualityController.text.isNotEmpty
      ? int.parse(qualityController.text)
      : null;
  final int? limit = limitController.text.isNotEmpty
      ? int.parse(limitController.text)
      : null;
  onPick(width, height, quality, limit);
}
