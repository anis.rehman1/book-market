import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/config/images/GetImage.dart';
import 'package:book_selling_app/config/images/ImageName.dart';
import 'package:book_selling_app/config/routes/app_routes.dart';
import 'package:book_selling_app/config/ui_components/CommonText.dart';
import 'package:book_selling_app/config/ui_components/decoration.dart';
import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:book_selling_app/config/util/PreferenceKeys.dart';
import 'package:book_selling_app/config/util/SharedPreferenceUtil.dart';
import 'package:book_selling_app/features/books/domain/entities/Book.dart';
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:get/get.dart';

import '../providers/HomeController.dart';

HomeView(HomeController controller) {
  return SingleChildScrollView(
    child: GestureDetector(
      onTap: () {
        FocusScope.of(buildContext!).requestFocus(FocusNode());
      },
    child:
    Column(
      children: [
        sizeBox15,
        Row(
          children: [
            const CommonText(
              value: "Hi, ",
              textColor: black,
              fontFamilyName: "Poppins",
              fontSize: 24,
            ),
            CommonText(
              value: Preference.getString(PreferenceKeys.USER_NAME) ?? "User",
              fontFamilyName: "Poppins",
              textColor: secondary,
              fontSize: 24,
            ),
            sizeBox15,
          ],
        ),
        sizeBox15,
        const SizedBox(
          width: double.maxFinite,
          child: CommonText(
            value: "Time to get new book for today",
            textAlign: TextAlign.left,
            textColor: disableGrayColor,
          ),
        ),
        sizeBox15,
        Stack(alignment: Alignment.centerRight, children: [

          TypeAheadField<Book>(
            suggestionsCallback: (search) => controller.getBooks(search),
            hideOnEmpty: true,
            hideOnUnfocus: true,
            hideOnLoading: true,
            hideWithKeyboard: true,
            hideKeyboardOnDrag: true,
            builder: (context, controller, focusNode) {

              return TextField(
                  controller: controller,
                  focusNode: focusNode,
                  autofocus: false,
                  decoration: InputDecoration(
                    fillColor: lightGrayBackground,
                    hintStyle: const TextStyle(color: grayTextColorLight), // Change this to your desired color
                    filled: true,
                    contentPadding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(32),
                        gapPadding: 0),
                    labelText: 'Books/Author',
                  ));
            },
            itemBuilder: (context, book) {
              return ListTile(
                title: Text(book.name ?? ""),
                subtitle: Text(book.author ?? ""),
              );
            },
            onSelected: (book) {
              Get.toNamed(AppRoutes.bookDetails, arguments: book);
            },
          ),
          Positioned(
              right: 16,
              child: GetImage.svgFromAssets(searchBarIcon, 30, 30)),
        ]),
        sizeBox15,
        SizedBox(
          height: getDeviceHeight(),
          child: ListView.builder(
            padding: EdgeInsets.only(bottom: getDeviceHeight() * .5),
            itemCount: controller.bookCategories.length,
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, indexParent) {
              return SizedBox(
                height: (deviceHeight ?? 0) * .28,
                child: Column(
                  children: [
                    SizedBox(
                      width: deviceWidth,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          CommonText(
                            value: controller.bookCategories[indexParent].name ?? "",
                            fontSize: 20,
                            textColor: black,
                            fontWeight: FontWeight.w600,
                            textAlign: TextAlign.left,
                          ),
                          InkWell(
                            onTap: (){
                              Get.toNamed(AppRoutes.allBooks, arguments: controller.bookCategories);
                            },
                            child: const CommonText(
                              value: "View All",
                              fontSize: 12,
                              textColor: primaryColor,
                              fontWeight: FontWeight.w400,
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ],
                      ),
                    ),
                    sizeBox20,
                    SizedBox(
                      height: (deviceHeight ?? 0) * .21,
                      width: getDeviceWidth(),
                      child: ListView.builder(
                        itemCount: controller.getBooksAsPerCategory(controller.bookCategories[indexParent].id ?? 0).length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          Book book = controller.getBooksAsPerCategory(controller.bookCategories[indexParent].id ?? 0)[index];
                          return BookItemView(book);
                        },
                      ),
                    ),
                  ],
                ),
              );;
            },
          ),
        ),
        sizeBox100,
      ],
    ),
  ));
}

BookItemView(book) {
  return Card(
    elevation: 1,
    surfaceTintColor: white,
    child: InkWell(
      onTap: () {
        Get.toNamed(AppRoutes.bookDetails, arguments: book);
      },
      child: SizedBox(
          width: (deviceWidth ?? 0) * .32,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Column(
              children: [
                GetImage.cachedImageFromNetwork(book.images?['image1'].toString() ?? "", 80, 120),
                sizeBox5,
                Text(
                  book.name ?? "",
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: const TextStyle(
                      color: black,
                      fontWeight: FontWeight.w400,
                      fontSize: 13
                  ),
                ),
                CommonText(
                  value: book.author ?? "",
                  fontSize: 11,
                ),
              ],
            ),
          )),
    ),
  );
}
