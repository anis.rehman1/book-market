import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/config/ui_components/CommonButton.dart';
import 'package:book_selling_app/config/ui_components/CommonText.dart';
import 'package:book_selling_app/config/ui_components/decoration.dart';
import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:book_selling_app/features/books/presentation/providers/AccountController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

AccountView(){
  var user = supabase.auth.currentSession?.user;
  var controller = Get.find<AccountController>();
  controller.fetchUserDetail();

  return  SingleChildScrollView(
    child: GetX<AccountController>(
      builder: (controller) =>
       Column(
        children: [
          sizeBox15,
          const Center(
            child: Column(
              children: [
                CommonText(value: "My Account", textColor: black, fontSize: 18, fontWeight: FontWeight.w600),
                Icon(
                  color: secondary,
                  Icons.account_circle_rounded,
                  size: 180,
                ),
              ],
            ),
          ),
          sizeBox20,
          const CommonText(value: "Phone", textColor: black, fontSize: 18, fontWeight: FontWeight.w600),
          sizeBox10,
          CommonText(value: user?.phone ?? "900001010", fontSize: 18, fontWeight: FontWeight.w400),
          sizeBox20,
          const CommonText(value: "Address",textColor: black, fontSize: 18, fontWeight: FontWeight.w600),
          sizeBox10,
          CommonText(value: controller.address.value ?? "", fontSize: 18, fontWeight: FontWeight.w400),
          sizeBox20,
          CommonButton(onClick: (){
            controller.logOut();
          },
              isEnabled: true,
              text: "Log Out")
        ],

      ),
    ),
  );
}
