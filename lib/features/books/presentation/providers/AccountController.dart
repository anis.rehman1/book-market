
import 'package:book_selling_app/config/routes/app_routes.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/GetUserUseCase.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/log_out_usecase.dart';
import 'package:get/get.dart';
import 'package:book_selling_app/features/authorization/domain/entities/user_entity.dart';



class AccountController extends GetxController{

  GetUser getUser;
  LogOutUseCase logOutUser;
  AccountController(this.getUser, this.logOutUser);
  RxBool isButtonEnabled = false.obs;
  RxString address = "".obs;

  var selectedCategory = "".obs;
  var image1 = "".obs;
  var image2 = "".obs;
  var image3 = "".obs;

  get bookCategoryNames => [];

  @override
  Future<void> onInit() async {
    super.onInit();

  }

  fetchUserDetail() async {
    UserModel user = await getUser.get();
    address.value = user.address ?? "";
  }

  logOut() async {
    logOutUser.call();
    Future.delayed(const Duration(seconds: 1),(){
      Get.offNamed(AppRoutes.login);
    });
  }

}