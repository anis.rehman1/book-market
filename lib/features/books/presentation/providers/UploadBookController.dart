
import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/add_user_usecase.dart';
import 'package:book_selling_app/features/books/domain/entities/Book.dart';
import 'package:book_selling_app/features/books/presentation/providers/HomeController.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../../domain/usecases/add_book_usecase.dart';


class UploadBookController extends GetxController{

  AddUser getUser;
  AddBook addBookUseCase;
  UploadBookController(this.getUser, this.addBookUseCase);
  RxBool isButtonEnabled = false.obs;
  late TextEditingController bookNameController;
  late TextEditingController authorController;
  late TextEditingController priceController;
  late TextEditingController descriptionController;
  late User? user;
  var selectedCategory = "".obs;
  var image1 = "".obs;
  var image2 = "".obs;
  var image3 = "".obs;

  get bookCategoryNames => [];

  @override
  void onInit() {
    super.onInit();
    bookNameController = TextEditingController();
    authorController = TextEditingController();
    priceController = TextEditingController();
    descriptionController = TextEditingController();
    user = supabase.auth.currentUser;
  }

  List<String> getBookCategoryText(){
    var bookCategories = Get.find<HomeController>().bookCategories;
    print(">>>categoty ${bookCategories.length}");
    List<String> list = [];
    for(var items in bookCategories){
      list.add(items.name ?? "");
    }
    print(">>>categoty ${list.length}");
    print(">>>list ${list.toString()}");

    return list;
  }

  bool validateBookName() {
    print("validating number");
    return bookNameController.text.isNotEmpty;
  }

  bool validateAuthorName() {
    return authorController.text.isNotEmpty;
  }

  bool validateDescription() {
    return descriptionController.text.isNotEmpty;
  }

  bool validatePrice() {
    return descriptionController.text.isNotEmpty;
  }

  enableButton(){
    isButtonEnabled.value = validateAuthorName() && validateBookName()
        && validateDescription() && validatePrice() && image1.value.isNotEmpty &&
    image2.value.isNotEmpty && image3.value.isNotEmpty;
  }

  uploadBook(){
    print("Uploading..");
    Map<String, dynamic>? images = {};

// Adding values to the map
    images["image1"] = image1.value;
    images["image2"] = image2.value;
    images["image3"] = image3.value;

    Book book = Book(
          0,
          "0",
        bookNameController.text,
        authorController.text,
        descriptionController.text,
        0,
        0.0,
        getCategoryIdFromCategory(),
        priceController.text ,
        user?.id,
        images,
        1,
        userAddress?.locality
      );
      try{
        addBookUseCase.call(book);
        Future.delayed(const Duration(milliseconds: 1500), () {
          Get.find<HomeController>().selectBottomItem(0);
        });
    Get.find<HomeController>().selectBottomItem(0);
      }
      catch(e){
        Get.find<HomeController>().selectBottomItem(0);
        print("error>>>");
      }

  }

  int? getCategoryIdFromCategory() {
    var bookCategories = Get.find<HomeController>().bookCategories;
    for(var items in bookCategories){
        if(items.name == selectedCategory.value){
          return items.id;
        }
    }
    return 1;
  }
}