
import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/features/authorization/domain/entities/AddressData.dart';
import 'package:book_selling_app/features/authorization/domain/entities/user_entity.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/UpdateLatLon.dart';
import 'package:book_selling_app/features/authorization/domain/usecases/add_user_usecase.dart';
import 'package:book_selling_app/features/books/data/repositories/book_repository_imp.dart';
import 'package:book_selling_app/features/books/domain/entities/BookCategory.dart';
import 'package:book_selling_app/features/location/data/repository/PinCodeRepositoryImpl.dart';
import 'package:flutter/services.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../../../authorization/domain/usecases/GetUserUseCase.dart';
import '../../domain/entities/Book.dart';

class HomeController extends GetxController{

  RxString appBarAddress = "Delivering to".obs;
  RxInt tabSelectedIndex = 0.obs;
  RxBool isAvailableAtLocation = true.obs;
  RxBool booksDataFetched = false.obs;
  AddUser addUser;
  UpdateLatLon updateLatLon;
  GetUser userDetails;
  BookRepositoryImp bookRepositoryImp;
  LocalityRepositoryImpl pinCodeRepositoryImp;
  var book = [].obs;
  List<Book> bookList = [];
  List<BookCategory> bookCategories = [];
  List<BookType> bookTypes = [];

  HomeController(this.addUser, this.userDetails, this.bookRepositoryImp, this.pinCodeRepositoryImp, this.updateLatLon);

  get bookCategoryNames => [];
  @override
  void onInit() {
    super.onInit();
  }

  Future<bool> _isLocationServiceEnabled() async {
    return await Geolocator.isLocationServiceEnabled();
  }

  /// Determine the current position of the device.
  Future<bool> _requestPermission() async {
    LocationPermission permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        print(">>>>>service denied");
        return Future.error('Location permissions are denied');
      }
    }
    return permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse;
  }

  selectBottomItem(int index) {
      tabSelectedIndex.value = index;
  }

  getBooks(String search) {

    List<Book> temp = [];
    for (var item in bookList) {
      String name = item.name ?? "";
      String author = item.author ?? "";
      if(name.toLowerCase().contains(search) || author.toLowerCase().contains(search)){
          temp.add(item);
      }
    }
    return book.value = temp;
  }

  List<Book> getTrending() {
    List<Book> temp = [];
    for (var item in bookList) {
      int type = item.type ?? 0;
      if(type == 1){
        temp.add(item);
      }
    }
    return temp;
  }

  List<Book> getLatest() {
    List<Book> temp = [];
    for (var item in bookList) {
      int type = item.type ?? 0;
      if(type == 2){
        temp.add(item);
      }
    }
    return temp;
  }

  List<String> getBookCategoryText(){
    List<String> list = [];
    for(var items in bookCategories){
      list.add(items.name ?? "");
    }
    return list;
  }

  List<Book> getBooksAsPerCategory(int categoryId){
    List<Book> list = [];
    for(var book in bookList){
      if(book.category == categoryId) {
        list.add(book);
      }
    }
    return list;
  }

  void onBackPressed() {
    print(tabSelectedIndex.value);
      switch(tabSelectedIndex.value){
        case 0:
          print(">>>> back");
          SystemNavigator.pop();
          break;
        case 1:
          tabSelectedIndex.value = 0;
          break;
        case 2:
          tabSelectedIndex.value = 0;
          break;
      }
  }

  Future<void> checkAvailableAtLocation(String? locality) async {
    isAvailableAtLocation.value = await pinCodeRepositoryImp.isAvailableAtLocality(locality ?? "");
  }

  Future<void> fetchBooksData() async {
    bookList = await bookRepositoryImp.fetchBooksAsPerLocality();
    bookCategories = await bookRepositoryImp.getBookCategory();
    bookTypes = await bookRepositoryImp.getBookType();
    booksDataFetched.value = true;
  }

  Future<void> updateAddress(AddressData addressData) async {
    List<Placemark> placemarks = await placemarkFromCoordinates(addressData.latitude ?? 0,
        addressData.longitude ?? 0);

    if(placemarks.isNotEmpty){
        setAddressAndFetchBooks(placemarks);
    }
    updateLatLon.update(addressData);
  }

  void setAddressAndFetchBooks(List<Placemark> placemarks) {
    userAddress = placemarks.first;
    print("Location ${placemarks.toString()}");
    appBarAddress.value = "${placemarks.first.street}, ${placemarks.first.subLocality}, ${placemarks.first.locality}";
    checkAvailableAtLocation(placemarks.first.locality);
    if(isAvailableAtLocation.value) {
      fetchBooksData();
    }
  }

  Future<void> getUserData() async {

    UserModel? userDetail = await userDetails.get();
    if(await _isLocationServiceEnabled() == false){
      return;
    }

    var permissionGiven = await _requestPermission();
    if(!permissionGiven) {
      return;
    }

    List<Placemark> placeMarks;
    if(userDetail == null){
      var position = await Geolocator.getCurrentPosition();
      placeMarks = await placemarkFromCoordinates(position.latitude, position.longitude);
      addUser.callAddUser(LatLng(position.latitude, position.longitude), placeMarks.first);
    }
    else{
      placeMarks = await placemarkFromCoordinates(userDetail.latitude ?? 0, userDetail.longitude ?? 0);
    }

    if(placeMarks.isNotEmpty){
      setAddressAndFetchBooks(placeMarks);
    }
  }
}