import 'package:book_selling_app/features/books/domain/entities/BookCategory.dart';
import 'package:book_selling_app/features/books/presentation/providers/HomeController.dart';
import 'package:get/get.dart';

import '../../domain/entities/Book.dart';

class AllBookListController extends GetxController {
  AllBookListController();

  RxInt selectedIndex = RxInt(0);
  RxList categoryList = RxList();
  RxList<Book> bookList = RxList();

  @override
  void onInit() {
    super.onInit();
  }

  void setBookCategories(List<BookCategory> arguments) {
    List<BookCategory> list = [BookCategory(-1, "", "All")];
    list.addAll(arguments);
    categoryList.value = list;

    // Setting books also
    bookList.value = Get.find<HomeController>().bookList;
  }

  void showBooksAsPerCategory(int index, int? categoryId) {
    selectedIndex.value= index;
    if (categoryId == -1) {
      bookList.value = Get.find<HomeController>().bookList;
      return;
    }

    List<Book> tempList = [];
    for (Book item in Get.find<HomeController>().bookList) {
      if (item.category == categoryId) {
        tempList.add(item);
      }
    }
    bookList.value = tempList;
  }
}
