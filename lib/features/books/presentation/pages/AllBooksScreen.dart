import 'package:book_selling_app/config/constants.dart';
import 'package:book_selling_app/features/books/presentation/providers/AllBookListController.dart';
import 'package:book_selling_app/features/books/presentation/providers/HomeController.dart';
import 'package:book_selling_app/features/books/presentation/widgets/HomeView.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../domain/entities/Book.dart';
import '../../domain/entities/BookCategory.dart';

class AllBooksScreen extends StatelessWidget {
  const AllBooksScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(AllBookListController());
    Get.find<AllBookListController>()
        .setBookCategories(Get.arguments as List<BookCategory>);
    return Scaffold(
      appBar: AppBar(title: const Text("Books")),
      body: Column(
        children: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                height: 30,
                child: GetX<AllBookListController>(
                  builder: (controller) => ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: controller.categoryList.length,
                    itemBuilder: (context, index) {
                      BookCategory category = controller.categoryList[index];
                      return GetX<AllBookListController>(
                        builder: (controller) => Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 0),
                          // Padding for spacing
                          margin: const EdgeInsets.only(right: 10.0),
                          // Margin for spacing between items
                          decoration: BoxDecoration(
                            color: controller.selectedIndex.value == index
                                ? Colors.blue
                                : Colors.grey,
                            borderRadius: BorderRadius.circular(
                                16.0), // Set the corner radius
                          ),
                          child: Center(
                            child: InkWell(
                              onTap: () {
                                controller.showBooksAsPerCategory(
                                    index, category.id);
                              },
                              child: Text(
                                category.name ?? "",
                                style: const TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              )),
          SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(children: [
              GetX<AllBookListController>(
                builder: (controller) => SizedBox(
                  height: getDeviceHeight() * .8,
                  child: GridView.builder(
                    shrinkWrap: true,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, // number of items in each row
                      mainAxisSpacing: 8.0,
                      childAspectRatio: .66,
                      crossAxisSpacing: 2.0, // spacing between columns
                    ),
                    padding: EdgeInsets.fromLTRB(8.0, 8, 8, getDeviceHeight() * .2),
                    // padding around the grid
                    itemCount: controller.bookList.length,
                    // total number of items
                    itemBuilder: (context, index) {
                      Book book = controller.bookList.value[index];
                      return BookItemView(book);
                    },
                  ),
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
