import 'package:book_selling_app/config/routes/app_routes.dart';
import 'package:book_selling_app/config/ui_components/CommonText.dart';
import 'package:book_selling_app/config/ui_components/decoration.dart';
import 'package:book_selling_app/config/ui_components/theme_data.dart';
import 'package:book_selling_app/config/util/PreferenceKeys.dart';
import 'package:book_selling_app/config/util/SharedPreferenceUtil.dart';
import 'package:book_selling_app/features/authorization/presentation/widgets/Drawer.dart';
import 'package:book_selling_app/features/books/presentation/providers/HomeController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../config/constants.dart';
import '../widgets/AccountView.dart';
import '../widgets/HomeView.dart';
import '../widgets/UploadBookView.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  static final List<Widget> _pages = <Widget>[
    HomeView(Get.find<HomeController>()),
    UploadBookView(),
    AccountView(),
  ];

  @override
  Widget build(BuildContext context) {
    buildContext = context;
    print("Name>>>${Preference.getString(PreferenceKeys.USER_NAME)}");

    Get.find<HomeController>().getUserData();

    return GetX<HomeController>(
      builder: (controller) => PopScope(
        canPop: false,
        onPopInvoked: (didPop) {
          print("back pressed");
          controller.onBackPressed();
        },
        child: Scaffold(
          endDrawer: getDrawer(),
          appBar: AppBar(
            centerTitle: false,
            automaticallyImplyLeading: false,
            title: Column(
              children: [
                InkWell(
                  onTap: () async {
                    navigateToPickLocation(controller);
                  },
                  child: SizedBox(
                    width: deviceWidth! * .5,
                    child: CommonText(
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                      value: controller.appBarAddress.value ?? "",
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 0),
              child: controller.isAvailableAtLocation.value == false
                  ? Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CommonText(
                          fontWeight: FontWeight.w300,
                          fontSize: 20,
                          textAlign: TextAlign.center,
                          value:
                              "Sorry ${Preference.getString(PreferenceKeys.USER_NAME) ?? "User"}, This service is not available at your location",
                        ),
                        sizeBox10,
                        InkWell(
                          child: const Text(
                            "Change Location",
                            style: TextStyle(color: secondary,),
                          ),
                          onTap: () {
                            navigateToPickLocation(controller);
                          },
                        )
                      ],
                    ))
                  : controller.booksDataFetched.value
                      ? _pages.elementAt(controller.tabSelectedIndex.value)
                      : const Center(child: CircularProgressIndicator())),
          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: 'Home',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.sell_sharp),
                label: 'Sell',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Account',
              ),
            ],
            currentIndex: controller.tabSelectedIndex.value,
            onTap: (index) {
              controller.selectBottomItem(index);
            },
          ),
        ),
      ),
    );
  }

  void navigateToPickLocation(HomeController controller) {
    Get.toNamed(AppRoutes.pickLocation)?.then((result) async {
      if (result != null) {
        controller.updateAddress(result);
      } else {
        print("Received null");
      }
    });
  }
}
